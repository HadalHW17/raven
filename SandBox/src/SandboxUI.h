#pragma once
#include <Layers/UILayer.h>

class SandboxUI : public Raven::RUILayer
{
public:
	using super = Raven::RUILayer;
	
	virtual void OnAttach() override;

	void GUI();
};