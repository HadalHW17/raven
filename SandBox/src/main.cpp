#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <Shlwapi.h>

#include "Engine/Application.h"
#include "Sandbox.h"

#include <dxgidebug.h>



void ReportLiveObjects()
{
    IDXGIDebug1* dxgiDebug;
    DXGIGetDebugInterface1(0, IID_PPV_ARGS(&dxgiDebug));

    dxgiDebug->ReportLiveObjects(DXGI_DEBUG_ALL, DXGI_DEBUG_RLO_IGNORE_INTERNAL);
    dxgiDebug->Release();
}

int CALLBACK wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR lpCmdLine, int nCmdShow)
{
    int retCode = 0;

    // Set the working directory to the path of the executable.
    WCHAR path[MAX_PATH];

    Raven::Application::Create(hInstance);
    {
        std::shared_ptr<Sandbox> demo = std::make_shared<Sandbox>(L"Raven Cube demo", 1280, 720, true);
        retCode = Raven::Application::Get().Run(demo);
    }
	Raven::Application::Destroy();

    atexit(&ReportLiveObjects);

    return retCode;
}