#include "Sandbox.h"

#include "Engine/Application.h"
#include "Renderer/CommandQueue.h"
#include "Renderer/CommandList.h"
#include "Engine/Helpers.h"
#include "Renderer/Light.h"
#include "Renderer/Material.h"
#include "Engine/Window.h"
#include "Renderer/GUI.h"
#include "SandboxUI.h"
#include "Engine/SceneObject.h"

#include <wrl.h>
using namespace Microsoft::WRL;

#include <Engine/d3dx12.h>
#include <d3dcompiler.h>
#include <DirectXColors.h>
#include <DirectXMath.h>
#include <algorithm> // For std::clamp

using namespace DirectX;

#include <algorithm> // For std::min and std::max.
#if defined(min)
#undef min
#endif

#if defined(max)
#undef max
#endif

#include "TerrainClass.h"

struct Mat
{
	XMMATRIX ModelMatrix;
	XMMATRIX ModelViewMatrix;
	XMMATRIX InverseTransposeModelMatrix;
	XMMATRIX InverseTransposeModelViewMatrix;
	XMMATRIX ModelViewProjectionMatrix;
	XMMATRIX LightViewMatrix;
	XMMATRIX LightProjectionMatrix;
};


// Builds a look-at (world) matrix from a point, up and direction vectors.
XMMATRIX XM_CALLCONV LookAtMatrix(FXMVECTOR Position, FXMVECTOR Direction, FXMVECTOR Up)
{
	assert(!XMVector3Equal(Direction, XMVectorZero()));
	assert(!XMVector3IsInfinite(Direction));
	assert(!XMVector3Equal(Up, XMVectorZero()));
	assert(!XMVector3IsInfinite(Up));

	XMVECTOR R2 = XMVector3Normalize(Direction);

	XMVECTOR R0 = XMVector3Cross(Up, R2);
	R0 = XMVector3Normalize(R0);

	XMVECTOR R1 = XMVector3Cross(R2, R0);

	XMMATRIX M(R0, R1, R2, Position);

	return M;
}

Sandbox::Sandbox(const std::wstring& name, int width, int height, bool vSync)
	: super(name, width, height, vSync)
	, m_Forward(0)
	, m_Backward(0)
	, m_Left(0)
	, m_Right(0)
	, m_Up(0)
	, m_Down(0)
	, m_Pitch(0)
	, m_Yaw(0)
	, m_AnimateLights(false)
	, m_Shift(false)
	, m_Width(0)
	, m_Height(0)
	, m_RenderScale(1.0f)
{

	XMVECTOR cameraPos = XMVectorSet(0, 5, -20, 1);
	XMVECTOR cameraTarget = XMVectorSet(0, 5, 0, 1);
	XMVECTOR cameraUp = XMVectorSet(0, 1, 0, 0);

	m_Camera.set_LookAt(cameraPos, cameraTarget, cameraUp);
	m_Camera.set_Projection(45.0f, width / (float)height, 0.1f, 100.0f);

	m_pAlignedCameraData = (CameraData*)_aligned_malloc(sizeof(CameraData), 16);

	m_pAlignedCameraData->m_InitialCamPos = m_Camera.get_Translation();
	m_pAlignedCameraData->m_InitialCamRot = m_Camera.get_Rotation();
	m_pAlignedCameraData->m_InitialFov = m_Camera.get_FoV();

	// Initialize our renderer in the game constructor.
	//m_Renderer = std::make_unique<Raven::DeferredRenderPath>(Raven::DeferredRenderPath());
}

Sandbox::~Sandbox()
{
	_aligned_free(m_pAlignedCameraData);
}
bool Sandbox::LoadContent()
{
	auto device = Raven::Application::Get().GetDevice();
	auto commandQueue = Raven::Application::Get().GetCommandQueue(D3D12_COMMAND_LIST_TYPE_COPY);
	auto commandList = commandQueue->GetCommandList();

	// Initialize UI.
	PushLayer(new SandboxUI());

	// Initialize off screen render targets
	m_Renderer.SetUpRenderTarget(*commandList);

	//// Create a Cube mesh
	//m_CubeMesh = Raven::Mesh::CreateCube(*commandList);
	m_SphereMesh = Raven::Mesh::CreateSphere(*commandList);
	m_ConeMesh = Raven::Mesh::CreateCone(*commandList);
	//m_TorusMesh = Raven::Mesh::LoadFromFIle(*commandList, "Assets/Meshes/bun_zipper.ply", true);
	//m_PlaneMesh = Raven::Mesh::CreatePlane(*commandList);
	m_Sponza = Raven::SceneObject::LoadObject(*commandList, "Assets/Meshes/Sponza/sponza.fbx");
	m_Sponza->SetScale({0.01f, 0.01f, 0.01f});
	// Create an inverted (reverse winding order) cube so the insides are not clipped.
	//m_Terran = TerrainClass::GetTerrain(*commandList, (char*)"Assets/Textures/heightmap01.bmp");

	// Load some textures
	commandList->LoadTextureFromFile(m_DefaultTexture, L"Assets/Textures/DefaultWhite.bmp");
	//commandList->LoadTextureFromFile(m_DirectXTexture, L"Assets/Textures/Discord/RTX.png");
	//commandList->LoadTextureFromFile(m_EarthTexture, L"Assets/Textures/earth.dds");
	//commandList->LoadTextureFromFile(m_ToiletTexture, L"Assets/Textures/Discord/bog.png");
	//commandList->LoadTextureFromFile(m_MIPTexture, L"Assets/Textures/Discord/MIP.png");
	//commandList->LoadTextureFromFile(m_HaskellTexture, L"Assets/Textures/Discord/Haskell.png");
	//commandList->LoadTextureFromFile(m_KotlinTexture, L"Assets/Textures/Discord/koTLIN_loGO.png");
	//commandList->LoadTextureFromFile(m_PrologTexture, L"Assets/Textures/Discord/PrologForDiscordLogo.png");
	//commandList->LoadTextureFromFile(m_MonaLisaTexture, L"Assets/Textures/Mona_Lisa.jpg");
	
	//commandList->LoadTextureFromFile(m_GraceCathedralTexture, L"Assets/Textures/UV_Test_Pattern.png");

	m_Renderer.InitPSO(*commandList);

	auto fenceValue = commandQueue->ExecuteCommandList(commandList);
	commandQueue->WaitForFenceValue(fenceValue);

	return true;
}

void Sandbox::RescaleHDRRenderTarget(float scale)
{
	uint32_t width = static_cast<uint32_t>(m_Width * scale);
	uint32_t height = static_cast<uint32_t>(m_Height * scale);

	width = std::clamp<uint32_t>(width, 1, D3D12_REQ_TEXTURE2D_U_OR_V_DIMENSION);
	height = std::clamp<uint32_t>(height, 1, D3D12_REQ_TEXTURE2D_U_OR_V_DIMENSION);

	GetRenderTarget().Resize(width, height);
}

bool Sandbox::OnResize(Raven::WindowResizedEvent& e)
{
	super::OnResize(e);

	if (m_Width != e.get_width() || m_Height != e.get_height())
	{
		m_Width = std::max(1, e.get_width());
		m_Height = std::max(1, e.get_height());

		float fov = m_Camera.get_FoV();
		float aspectRatio = m_Width / (float)m_Height;
		m_Camera.set_Projection(fov, aspectRatio, 0.1f, 100.0f);

		RescaleHDRRenderTarget(m_RenderScale);
	}
	return true;
}


void Sandbox::UnloadContent()
{
}


bool Sandbox::OnUpdate(Raven::AppUpdateEvent& e)
{
	static uint64_t frameCount = 0;
	static double totalTime = 0.0;

	super::OnUpdate(e);

	totalTime += e.m_ElapsedTime;
	frameCount++;

	if (totalTime > 1.0)
	{
		g_FPS = frameCount / totalTime;

		char buffer[512];
		sprintf_s(buffer, "FPS: %f\n", g_FPS);
		OutputDebugStringA(buffer);

		frameCount = 0;
		totalTime = 0.0;
	}

	// Update the camera.
	float speedMultipler = (m_Shift ? 16.0f : 4.0f);

	XMVECTOR cameraTranslate = XMVectorSet(m_Right - m_Left, 0.0f, m_Forward - m_Backward, 1.0f) * speedMultipler * static_cast<float>(e.m_ElapsedTime);
	XMVECTOR cameraPan = XMVectorSet(0.0f, m_Up - m_Down, 0.0f, 1.0f) * speedMultipler * static_cast<float>(e.m_ElapsedTime);
	m_Camera.Translate(cameraTranslate, Raven::Space::Local);
	m_Camera.Translate(cameraPan, Raven::Space::Local);

	XMVECTOR cameraRotation = XMQuaternionRotationRollPitchYaw(XMConvertToRadians(m_Pitch), XMConvertToRadians(m_Yaw), 0.0f);
	m_Camera.set_Rotation(cameraRotation);

	XMMATRIX viewMatrix = m_Camera.get_ViewMatrix();

	const int numPointLights = 4;
	const int numSpotLights = 1;

	static const XMVECTORF32 LightColors[] =
	{
		Colors::White, Colors::Orange, Colors::Yellow, Colors::Green, Colors::Blue, Colors::Indigo, Colors::Violet, Colors::White
	};

	static float lightAnimTime = 0.0f;
	if (m_AnimateLights)
	{
		lightAnimTime += static_cast<float>(e.m_ElapsedTime) * 0.5f * XM_PI;
	}

	const float radius = 3.0f;
	const float offset = 2.0f * XM_PI / numPointLights;
	const float offset2 = offset + (offset / 2.0f);

	// Setup the light buffers.
	m_PointLights.resize(numPointLights);
	for (int i = 0; i < numPointLights; ++i)
	{
		PointLight& l = m_PointLights[i];

		l.PositionWS = {
			static_cast<float>(std::sin(lightAnimTime + offset * i)) * radius,
			9.0f,
			static_cast<float>(std::cos(lightAnimTime + offset * i)) * radius,
			1.0f
		};
		XMVECTOR positionWS = XMLoadFloat4(&l.PositionWS);
		XMVECTOR positionVS = XMVector3TransformCoord(positionWS, viewMatrix);
		XMStoreFloat4(&l.PositionVS, positionVS);

		l.Color = XMFLOAT4(LightColors[i]);
		l.Intensity = 1.0f;
		l.Attenuation = 0.0f;
	}

	m_SpotLights.resize(numSpotLights);
	for (int i = 0; i < numSpotLights; ++i)
	{
		SpotLight& l = m_SpotLights[i];

		l.PositionWS = {
			static_cast<float>(std::sin(lightAnimTime + offset * i + offset2)) * radius,
			9.0f,
			static_cast<float>(std::cos(lightAnimTime + offset * i + offset2)) * radius,
			1.0f
		};
		XMVECTOR positionWS = XMLoadFloat4(&l.PositionWS);
		XMVECTOR positionVS = XMVector3TransformCoord(positionWS, viewMatrix);
		XMStoreFloat4(&l.PositionVS, positionVS);

		XMVECTOR directionWS = XMVector3Normalize(XMVectorSetW(XMVectorNegate(positionWS), 0));
		XMVECTOR directionVS = XMVector3Normalize(XMVector3TransformNormal(directionWS, viewMatrix));
		XMStoreFloat4(&l.DirectionWS, directionWS);
		XMStoreFloat4(&l.DirectionVS, directionVS);

		l.Color = XMFLOAT4(LightColors[numPointLights + i]);
		l.Intensity = 1.0f;
		l.SpotAngle = XMConvertToRadians(45.0f);
		l.Attenuation = 0.0f;
	}
	return true;
}


void XM_CALLCONV ComputeMatrices(FXMMATRIX model, CXMMATRIX view, CXMMATRIX viewProjection, Mat& mat)
{
	mat.ModelMatrix = model;
	mat.ModelViewMatrix = model * view;
	mat.InverseTransposeModelMatrix = XMMatrixTranspose(XMMatrixInverse(nullptr, model));
	mat.InverseTransposeModelViewMatrix = XMMatrixTranspose(XMMatrixInverse(nullptr, mat.ModelViewMatrix));
	mat.ModelViewProjectionMatrix = model * viewProjection;
}

bool Sandbox::OnRender(Raven::AppRenderEvent& e)
{
	super::OnRender(e);

	auto commandQueue = Raven::Application::Get().GetCommandQueue(D3D12_COMMAND_LIST_TYPE_DIRECT);
	auto commandList = commandQueue->GetCommandList();

	// Clear the render target.
	m_Renderer.ClearRenderTarget(*commandList);



	// Upload lights
	Raven::LightProperties lightProps;
	lightProps.NumPointLights = static_cast<uint32_t>(m_PointLights.size());
	lightProps.NumSpotLights = static_cast<uint32_t>(m_SpotLights.size());

	std::vector<Raven::SceneObject*> objects;
	objects.push_back(m_Sponza.get());


	// Pre render stage computes shadow map and sets up some parameters for the actual rendering stage
	m_Renderer.PreRender(*commandList, m_Camera,m_SpotLights, objects);

		// Draw the earth sphere
	XMMATRIX translationMatrix;
	XMMATRIX rotationMatrix;
	XMMATRIX scaleMatrix;
	XMMATRIX worldMatrix;
	XMMATRIX viewMatrix = m_Camera.get_ViewMatrix();
	XMMATRIX viewProjectionMatrix = viewMatrix * m_Camera.get_ProjectionMatrix();
	Mat matrices;
	// Process all of the scene geometry after pre rendering stage.
	{

		ComputeMatrices(m_Sponza->GetWorldMatrix(), viewMatrix, viewProjectionMatrix, matrices);

		matrices.LightViewMatrix = m_SpotLights[0].MakeViewMatrix();
		matrices.LightProjectionMatrix = m_SpotLights[0].MakeProjectionMatrix(0.1f, 100.f);
		commandList->SetGraphicsDynamicConstantBuffer(0, matrices);
		commandList->SetGraphicsDynamicConstantBuffer(1, Material::White);
		commandList->SetGraphics32BitConstants(3, m_SpotLights[0].PositionWS);

		for (auto& mesh : m_Sponza->GetMeshes())
		{
			commandList->SetShaderResourceView(2, 0, mesh->GetTexture(), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
			commandList->SetShaderResourceView(2, 1, mesh->GetNormalMap(), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
			mesh->Render(*commandList);
		}
	}
	//// Draw shapes to visualize the position of the lights in the scene.
	Material lightMaterial;
	//// No specular
	lightMaterial.Specular = { 0, 0, 0, 1 };
	//for (const auto& l : m_PointLights)
	//{
	//	lightMaterial.Emissive = l.Color;
	//	XMVECTOR lightPos = XMLoadFloat4(&l.PositionWS);
	//	worldMatrix = XMMatrixTranslationFromVector(lightPos);
	//	ComputeMatrices(worldMatrix, viewMatrix, viewProjectionMatrix, matrices);

	//	commandList->SetGraphicsDynamicConstantBuffer(0, matrices);
	//	commandList->SetGraphicsDynamicConstantBuffer(1, lightMaterial);

	//	m_SphereMesh->Render(*commandList);
	//}

	for (const auto& l : m_SpotLights)
	{
		lightMaterial.Emissive = l.Color;
		XMVECTOR lightPos = XMLoadFloat4(&l.PositionWS);
		XMVECTOR lightDir = XMLoadFloat4(&l.DirectionWS);
		XMVECTOR up = XMVectorSet(0, 1, 0, 0);

		// Rotate the cone so it is facing the Z axis.
		rotationMatrix = XMMatrixRotationX(XMConvertToRadians(-90.0f));
		worldMatrix = rotationMatrix * LookAtMatrix(lightPos, lightDir, up);

		ComputeMatrices(worldMatrix, viewMatrix, viewProjectionMatrix, matrices);

		commandList->SetGraphicsDynamicConstantBuffer(0, matrices);
		commandList->SetGraphicsDynamicConstantBuffer(1, lightMaterial);
		commandList->SetShaderResourceView(2, 0, m_DefaultTexture, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);

		m_ConeMesh->Render(*commandList);
	}

	// Perform rendering stage of the graphics path (this is mainly shading).
	m_Renderer.Render(*commandList, lightProps, m_PointLights, m_SpotLights);

	// Do all of the post processing.
	m_Renderer.PostRender(*commandList);

	// Execute the command list and present render target to the screen.
	commandQueue->ExecuteCommandList(commandList);
	m_pWindow->Present();

	return true;
}

static bool g_AllowFullscreenToggle = true;

bool Sandbox::OnKeyPressed(Raven::KeyPressedEvent& e)
{
	super::OnKeyPressed(e);
	if (!ImGui::GetIO().WantCaptureKeyboard)
	{
		switch (e.m_Key)
		{
		case KeyCode::Escape:
			Raven::Application::Get().Quit(0);
			break;
		case KeyCode::Enter:
			if (e.m_Alt)
			{
		case KeyCode::F11:
			if (g_AllowFullscreenToggle)
			{
				m_pWindow->ToggleFullscreen();
				g_AllowFullscreenToggle = false;
			}
			break;
			}
		case KeyCode::V:
			m_pWindow->ToggleVSync();
			break;
		case KeyCode::R:
			// Reset camera transform
			m_Camera.set_Translation(m_pAlignedCameraData->m_InitialCamPos);
			m_Camera.set_Rotation(m_pAlignedCameraData->m_InitialCamRot);
			m_Camera.set_FoV(m_pAlignedCameraData->m_InitialFov);
			m_Pitch = 0.0f;
			m_Yaw = 0.0f;
			break;
		case KeyCode::Up:
		case KeyCode::W:
			m_Forward = 1.0f;
			break;
		case KeyCode::Left:
		case KeyCode::A:
			m_Left = 1.0f;
			break;
		case KeyCode::Down:
		case KeyCode::S:
			m_Backward = 1.0f;
			break;
		case KeyCode::Right:
		case KeyCode::D:
			m_Right = 1.0f;
			break;
		case KeyCode::Q:
			m_Down = 1.0f;
			break;
		case KeyCode::E:
			m_Up = 1.0f;
			break;
		case KeyCode::Space:
			m_AnimateLights = !m_AnimateLights;
			break;
		case KeyCode::ShiftKey:
			m_Shift = true;
			break;
		}
	}
	return true;
}

bool Sandbox::OnKeyReleased(Raven::KeyReleaseEvent& e)
{
	super::OnKeyReleased(e);
	if (!ImGui::GetIO().WantCaptureKeyboard)
	{
		switch (e.m_Key)
		{
		case KeyCode::Enter:
			if (e.m_Alt)
			{
		case KeyCode::F11:
			g_AllowFullscreenToggle = true;
			}
			break;
		case KeyCode::Up:
		case KeyCode::W:
			m_Forward = 0.0f;
			break;
		case KeyCode::Left:
		case KeyCode::A:
			m_Left = 0.0f;
			break;
		case KeyCode::Down:
		case KeyCode::S:
			m_Backward = 0.0f;
			break;
		case KeyCode::Right:
		case KeyCode::D:
			m_Right = 0.0f;
			break;
		case KeyCode::Q:
			m_Down = 0.0f;
			break;
		case KeyCode::E:
			m_Up = 0.0f;
			break;
		case KeyCode::B:
			bump = !bump;
			break;
		case KeyCode::ShiftKey:
			m_Shift = false;
			break;
		}
	}
	return true;
}

bool Sandbox::OnMouseMoved(Raven::MouseMovedEvent& e)
{
	super::OnMouseMoved(e);

	const float mouseSpeed = 0.1f;
	if (!ImGui::GetIO().WantCaptureMouse)
	{
		if (e.m_LeftButton)
		{
			m_Pitch -= e.m_RelY * mouseSpeed;

			m_Pitch = std::clamp(m_Pitch, -90.0f, 90.0f);

			m_Yaw -= e.m_RelX * mouseSpeed;
		}
	}
	return true;
}


bool Sandbox::OnMouseWheel(Raven::MouseScrolledEvent& e)
{
	if (!ImGui::GetIO().WantCaptureMouse)
	{
		auto fov = m_Camera.get_FoV();

		fov -= e.m_WheelDelta;
		fov = std::clamp(fov, 12.0f, 90.0f);

		m_Camera.set_FoV(fov);

		char buffer[256];
		sprintf_s(buffer, "FoV: %f\n", fov);
		OutputDebugStringA(buffer);
	}
	return true;
}
