#pragma once

#include <Renderer/CommandList.h>
#include <Renderer/VertexBuffer.h>
#include <Renderer/IndexBuffer.h>
#include <DirectXMath.h>
#include <d3d12.h>


class TerrainClass
{

public:
	TerrainClass();
	TerrainClass(const TerrainClass& otherTerrain);
	~TerrainClass();

	static std::unique_ptr<TerrainClass> GetTerrain(Raven::CommandList& commandList, char *filename);
	void Render(Raven::CommandList& commandList);
	void Shutdown();
	inline const UINT GetIndexCount() const { return m_indexCount; }


private:
	bool Initialize(Raven::CommandList& commandList, char* filename);
	bool InitializeBuffers(Raven::CommandList& commandList, char* filename);
	bool LoadHeightMap(char *filename);
	void NormalizeHeightmap();

private:
	struct VertexData
	{
		DirectX::XMFLOAT3 position;
		DirectX::XMFLOAT3 normal;
		DirectX::XMFLOAT2 textureCoordinate;
	};

	struct HeightMapData
	{
		float x, y, z;
	};

	std::vector<HeightMapData> m_HeightMap;
	size_t m_width, m_heigth;
	UINT m_indexCount;
	Raven::VertexBuffer m_vertexBuffer;
	Raven::IndexBuffer m_indexBuffer;
};