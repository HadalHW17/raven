#pragma once


#include "Renderer/Camera.h"
#include "Engine/Game.h"
#include "Renderer/IndexBuffer.h"
#include "Renderer/Light.h"
#include "Engine/Window.h"
#include "Renderer/Mesh.h"
#include "Renderer/Skybox.h"
#include "Renderer/RenderTarget.h"
#include "Renderer/RootSignature.h"
#include "Renderer/Texture.h"
#include "Renderer/VertexBuffer.h"

#include "Renderer/Tonemapping.h"
#include <Events/ApplicationEvent.h>
#include "Engine/SceneObject.h"
#include "Renderer/DeferredRenderPath.h"

class Sandbox : public Raven::Game
{
public:
	using super = Raven::Game;

	Sandbox(const std::wstring& name, int width, int height, bool vSync = false);
	virtual ~Sandbox();

	/**
	 *  Load content required for the demo.
	 */
	virtual bool LoadContent() override;

	/**
	 *  Unload demo specific content that was loaded in LoadContent.
	 */
	virtual void UnloadContent() override;

	void RescaleHDRRenderTarget(float scale);

	inline Raven::RenderTarget& GetRenderTarget() { return m_Renderer.GetRenderTarget(); }

	// Scale the HDR render target to a fraction of the window size.
	float m_RenderScale;

protected:
	/**
	 *  Update the game logic.
	 */
	virtual bool OnUpdate(Raven::AppUpdateEvent& e) override;

	/**
	 *  Render stuff.
	 */
	virtual bool OnRender(Raven::AppRenderEvent& e) override;

	/**
	 * Invoked by the registered window when a key is pressed
	 * while the window has focus.
	 */
	virtual bool OnKeyPressed(Raven::KeyPressedEvent& e) override;

	/**
	 * Invoked when a key on the keyboard is released.
	 */
	virtual bool OnKeyReleased(Raven::KeyReleaseEvent& e) override;

	/**
	 * Invoked when the mouse is moved over the registered window.
	 */
	virtual bool OnMouseMoved(Raven::MouseMovedEvent& e) override;

	/**
	 * Invoked when the mouse wheel is scrolled while the registered window has focus.
	 */
	virtual bool OnMouseWheel(Raven::MouseScrolledEvent& e) override;

	virtual bool OnResize(Raven::WindowResizedEvent& e) override;

private:
	// This is out main renderer.
	Raven::DeferredRenderPath m_Renderer;

	// Skybox.
	Raven::Skybox m_Skybox;


	// Some geometry to render.
	std::unique_ptr<Raven::Mesh> m_CubeMesh;
	std::unique_ptr<Raven::Mesh> m_SphereMesh;
	std::unique_ptr<Raven::Mesh> m_ConeMesh;
	std::unique_ptr<Raven::Mesh> m_TorusMesh;
	std::unique_ptr<Raven::Mesh> m_PlaneMesh;
	std::unique_ptr<class TerrainClass> m_Terran;
	std::unique_ptr<Raven::SceneObject> m_Sponza;


	Raven::Texture m_DefaultTexture;
	Raven::Texture m_DirectXTexture;
	Raven::Texture m_EarthTexture;
	Raven::Texture m_ToiletTexture;
	Raven::Texture m_HaskellTexture;
	Raven::Texture m_KotlinTexture;
	Raven::Texture m_MIPTexture;
	Raven::Texture m_PrologTexture;
	Raven::Texture m_MonaLisaTexture;


	Raven::Camera m_Camera;
	struct alignas(16) CameraData
	{
		DirectX::XMVECTOR m_InitialCamPos;
		DirectX::XMVECTOR m_InitialCamRot;
		float m_InitialFov;
	};
	CameraData* m_pAlignedCameraData;

	// Camera controller
	float m_Forward;
	float m_Backward;
	float m_Left;
	float m_Right;
	float m_Up;
	float m_Down;

	float m_Pitch;
	float m_Yaw;

	// Rotate the lights in a circle.
	bool m_AnimateLights;
	// Set to true if the Shift key is pressed.
	bool m_Shift;

	int m_Width;
	int m_Height;

	// Define some lights.
	std::vector<PointLight> m_PointLights;
	std::vector<SpotLight> m_SpotLights;

};