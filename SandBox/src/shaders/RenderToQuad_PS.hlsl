Texture2D<float4> InputTexture : register(t0);
SamplerState TextureSampler : register(s0);

float4 main(float2 TexCoord : TEXCOORD) : SV_TARGET
{
	float4 tex = InputTexture.Sample(TextureSampler, TexCoord);
	return tex;
}