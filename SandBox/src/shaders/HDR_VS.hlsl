struct Mat
{
    matrix ModelMatrix;
    matrix ModelViewMatrix;
	matrix InverseTransposeModelMatrix;
    matrix InverseTransposeModelViewMatrix;
    matrix ModelViewProjectionMatrix;
	matrix LightViewMatrix;
	matrix LightProjectionMatrix;
};

ConstantBuffer<Mat> MatCB : register(b0);

struct VertexPositionNormalTexture
{
    float3 Position : POSITION;
    float3 Normal   : NORMAL;
    float2 TexCoord : TEXCOORD;
	float3 Binormal : BINORMAL;
	float3 Tangent	: TANGENT;
};

struct VertexShaderOutput
{
    float4 PositionVS : POSITION;
    float3 NormalVS   : NORMAL;
    float2 TexCoord   : TEXCOORD;
    float4 LightViewPos   : TEXCOORD1;
    float3 LightPos   : TEXCOORD2;
	float3 Binormal : BINORMAL;
	float3 Tangent	: TANGENT;
    float4 Position   : SV_Position;
};

struct LightPos
{
	float4 pos;
};
ConstantBuffer<LightPos> LightPropertiesCB : register(b1);

VertexShaderOutput main(VertexPositionNormalTexture IN)
{
    VertexShaderOutput OUT;

    OUT.Position = mul( MatCB.ModelViewProjectionMatrix, float4(IN.Position, 1.f));
    OUT.PositionVS = mul( MatCB.ModelViewMatrix, float4(IN.Position, 1.f));
    OUT.NormalVS = mul((float3x3)MatCB.InverseTransposeModelMatrix, IN.Normal);
    OUT.Binormal = mul((float3x3)MatCB.InverseTransposeModelMatrix, IN.Binormal);
    OUT.Tangent = mul((float3x3)MatCB.InverseTransposeModelMatrix, IN.Tangent);
    OUT.TexCoord = IN.TexCoord;

	// Calculate the position of the vertice as viewed by the light source.
	OUT.LightViewPos = mul(MatCB.ModelMatrix, float4(IN.Position, 1.f));
	OUT.LightViewPos = mul(MatCB.LightViewMatrix, OUT.LightViewPos);
	OUT.LightViewPos = mul(MatCB.LightProjectionMatrix, OUT.LightViewPos);

	// Calculate the position of the vertex in the world.
	float4 worldPosition = mul(MatCB.ModelMatrix, float4(IN.Position, 1.f));

	// Determine the light position based on the position of the light and the position of the vertex in the world.
	OUT.LightPos = LightPropertiesCB.pos.xyz - worldPosition.xyz;

	// Normalize the light position vector.
	OUT.LightPos = normalize(OUT.LightPos);

    return OUT;
}