struct PixelShaderInput
{
	float4 PositionVS : POSITION;
	float3 NormalVS   : NORMAL;
	float2 TexCoord   : TEXCOORD;
	float4 LightViewPos   : TEXCOORD1;
	float3 LightPos   : TEXCOORD2;
	float3 Binormal : BINORMAL;
	float3 Tangent	: TANGENT;
};

struct PixelShaderOutput
{
	float4 Albedo : SV_Target0;
	float4 Normal : SV_Target1;
	float4 Diffuse : SV_Target2;
	float4 Specular : SV_Target3;
	float4 Position : SV_Target4;
};

struct Material
{
	float4 Emissive;
	//----------------------------------- (16 byte boundary)
	float4 Ambient;
	//----------------------------------- (16 byte boundary)
	float4 Diffuse;
	//----------------------------------- (16 byte boundary)
	float4 Specular;
	//----------------------------------- (16 byte boundary)
	float  SpecularPower;
	float3 Padding;
	//----------------------------------- (16 byte boundary)
	// Total:                              16 * 5 = 80 bytes
};


Texture2D<float3> DiffuseTexture[] : register(t2);
SamplerState LinearSampler : register(s0);

ConstantBuffer<Material> MaterialCB : register(b0, space1);

PixelShaderOutput main(PixelShaderInput IN)
{
	PixelShaderOutput OUT;
	OUT.Albedo = float4(DiffuseTexture[0].Sample(LinearSampler, IN.TexCoord), 1.f);
	OUT.Diffuse = float4(IN.LightPos, 1.f);
	OUT.Specular = IN.LightViewPos;
	float3 bumpMap = DiffuseTexture[1].Sample(LinearSampler, IN.TexCoord); // Normal map is written there for now.
	bumpMap = (bumpMap * 2.f) - 1.f;
	float3 bumpNormal = (bumpMap.x * IN.Tangent) + (bumpMap.y * IN.Binormal) + (bumpMap.z * IN.NormalVS);
	OUT.Normal = float4(normalize(bumpNormal), 1.f);
	OUT.Position = IN.PositionVS;
	return OUT;
}