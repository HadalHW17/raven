struct PixelShaderInput
{
	float4 PositionVS : POSITION;
	float3 NormalVS   : NORMAL;
	float2 TexCoord   : TEXCOORD;
};

float4 main( PixelShaderInput IN ) : SV_Target
{
    return float4(1,0,0,0);
}