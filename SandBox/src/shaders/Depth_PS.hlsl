struct PixelShaderInput
{
	float4 DepthBuffer : POSITION;
};

struct PixelShaderOutput
{
	float4 Position : SV_Target0;
};


float4 main(float4 DepthBuffer : POSITION) : SV_TARGET
{
	return DepthBuffer.z / DepthBuffer.w;
}