struct Mat
{
	matrix ModelViewProjectionMatrix;
};

ConstantBuffer<Mat> MatCB : register(b0);

struct VertexPositionNormalTexture
{
	float3 Position : POSITION;
};

struct VertexShaderOutput
{
	float4 DepthBuffer : POSITION; // Will store depth values.
	float4 Position   : SV_Position;
};


VertexShaderOutput main(VertexPositionNormalTexture IN)
{
	VertexShaderOutput OUT;

	// Calculate the position of the vertice as viewed by the observer.
	OUT.Position = mul(MatCB.ModelViewProjectionMatrix, float4(IN.Position, 1.f));

	// Store the position in a separate variable that wouldn't be change by the automatically.
	OUT.DepthBuffer = OUT.Position;

	return OUT;
}