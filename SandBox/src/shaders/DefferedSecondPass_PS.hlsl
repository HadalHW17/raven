struct PixelShaderInput
{
	float2 TexCoord   : TEXCOORD;
};

struct PixelShaderOutput
{
	//float4 ShadowMap :   SV_Target4;
	float4 ShadedImage : SV_Target5;
};

struct Material
{
	float4 Emissive;
	//----------------------------------- (16 byte boundary)
	float4 Ambient;
	//----------------------------------- (16 byte boundary)
	float4 Diffuse;
	//----------------------------------- (16 byte boundary)
	float4 Specular;
	//----------------------------------- (16 byte boundary)
	float  SpecularPower;
	float3 Padding;
	//----------------------------------- (16 byte boundary)
	// Total:                              16 * 5 = 80 bytes
};

struct PointLight
{
	float4 PositionWS; // Light position in world space.
	//----------------------------------- (16 byte boundary)
	float4 PositionVS; // Light position in view space.
	//----------------------------------- (16 byte boundary)
	float4 Color;
	//----------------------------------- (16 byte boundary)
	float       Intensity;
	float       Attenuation;
	float2      Padding;                // Pad to 16 bytes
	//----------------------------------- (16 byte boundary)
	// Total:                              16 * 4 = 64 bytes
};

struct SpotLight
{
	float4 PositionWS; // Light position in world space.
	//----------------------------------- (16 byte boundary)
	float4 PositionVS; // Light position in view space.
	//----------------------------------- (16 byte boundary)
	float4 DirectionWS; // Light direction in world space.
	//----------------------------------- (16 byte boundary)
	float4 DirectionVS; // Light direction in view space.
	//----------------------------------- (16 byte boundary)
	float4 Color;
	//----------------------------------- (16 byte boundary)
	float       Intensity;
	float       SpotAngle;
	float       Attenuation;
	float       Padding;                // Pad to 16 bytes.
	//----------------------------------- (16 byte boundary)
	// Total:                              16 * 6 = 96 bytes
};

struct LightProperties
{
	uint NumPointLights;
	uint NumSpotLights;
};

struct LightResult
{
	float4 Diffuse;
	float4 Specular;
};

ConstantBuffer<Material> MaterialCB : register(b0, space1);
ConstantBuffer<LightProperties> LightPropertiesCB : register(b1);

StructuredBuffer<PointLight> PointLights : register(t0);
StructuredBuffer<SpotLight> SpotLights : register(t1);
Texture2D FirstPassRes[]            : register(t2);

SamplerState LinearRepeatSampler    : register(s0);

float3 LinearToSRGB(float3 x)
{
	// This is exactly the sRGB curve
	//return x < 0.0031308 ? 12.92 * x : 1.055 * pow(abs(x), 1.0 / 2.4) - 0.055;

	// This is cheaper but nearly equivalent
	return x < 0.0031308 ? 12.92 * x : 1.13005 * sqrt(abs(x - 0.00228)) - 0.13448 * x + 0.005719;
}

float DoDiffuse(float3 N, float3 L)
{
	return max(0, dot(N, L));
}

float DoSpecular(float3 V, float3 N, float3 L)
{
	float3 R = normalize(reflect(-L, N));
	float RdotV = max(0, dot(R, V));

	return pow(RdotV, MaterialCB.SpecularPower);
}

float DoAttenuation(float attenuation, float distance)
{
	return 1.0f / (1.0f + attenuation * distance * distance);
}

float DoSpotCone(float3 spotDir, float3 L, float spotAngle)
{
	float minCos = cos(spotAngle);
	float maxCos = (minCos + 1.0f) / 2.0f;
	float cosAngle = dot(spotDir, -L);
	return smoothstep(minCos, maxCos, cosAngle);
}

LightResult DoPointLight(PointLight light, float3 V, float3 P, float3 N)
{
	LightResult result;
	float3 L = (light.PositionVS.xyz - P);
	float d = length(L);
	L = L / d;

	float attenuation = DoAttenuation(light.Attenuation, d);

	result.Diffuse = DoDiffuse(N, L) * attenuation * light.Color * light.Intensity;
	result.Specular = DoSpecular(V, N, L) * attenuation * light.Color * light.Intensity;

	return result;
}

LightResult DoSpotLight(SpotLight light, float3 V, float3 P, float3 N)
{
	LightResult result;
	float3 L = (light.PositionVS.xyz - P);
	float d = length(L);
	L = L / d;

	float attenuation = DoAttenuation(light.Attenuation, d);

	float spotIntensity = DoSpotCone(light.DirectionVS.xyz, L, light.SpotAngle);

	result.Diffuse = DoDiffuse(N, L) * attenuation * spotIntensity * light.Color * light.Intensity;
	result.Specular = DoSpecular(V, N, L) * attenuation * spotIntensity * light.Color * light.Intensity;

	return result;
}

LightResult DoLighting(float3 P, float3 N)
{
	uint i;

	// Lighting is performed in view space.
	float3 V = normalize(-P);

	LightResult totalResult = (LightResult)0;

	//for (i = 0; i < LightPropertiesCB.NumPointLights; ++i)
	//{
	//	LightResult result = DoPointLight(PointLights[i], V, P, N);

	//	totalResult.Diffuse += result.Diffuse;
	//	totalResult.Specular += result.Specular;
	//}

	for (i = 0; i < LightPropertiesCB.NumSpotLights; ++i)
	{
		LightResult result = DoSpotLight(SpotLights[i], V, P, N);

		totalResult.Diffuse += result.Diffuse;
		totalResult.Specular += result.Specular;
	}

	return totalResult;
}


PixelShaderOutput main(float2 TexCoord : TEXCOORD)
{
	PixelShaderOutput output;
	float2 projectTexCoord;
	float depthValue;
	float bias = 0.001f;
	float lightDepthValue;
	float lightIntensity;

	float4 emissive = MaterialCB.Emissive;
	float4 ambient = MaterialCB.Ambient;
	float4 lightPos = FirstPassRes[2].Sample(LinearRepeatSampler, TexCoord);	 // Light position.
	float4 lightViewPos = FirstPassRes[3].Sample(LinearRepeatSampler, TexCoord); // Light view position.
	float4 texColor = FirstPassRes[0].Sample(LinearRepeatSampler, TexCoord);
	output.ShadedImage = float4(0.15f, 0.15f, 0.15f, 1.f);
	// Calculate the projected texture coordinates.
	projectTexCoord.x = lightViewPos.x / lightViewPos.w / 2.0f + 0.5f;
	projectTexCoord.y = -lightViewPos.y / lightViewPos.w / 2.0f + 0.5f;
	// Determine if the projected coordinates are in the 0 to 1 range.  If so then this pixel is in the view of the light.
	if ((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
	{
		uint3 index = uint3(projectTexCoord.x, projectTexCoord.y, 0);
		// Get the shadow map depth at the projected texture coordinate location.
		depthValue = FirstPassRes[4].Sample(LinearRepeatSampler, projectTexCoord).r;

		// Calculate the depth of the light.
		lightDepthValue = lightViewPos.z / lightViewPos.w;

		// Subtract the bias from the lightDepthValue.
		lightDepthValue = lightDepthValue - bias;
		if (lightDepthValue < depthValue)
		{
			float lightInt = saturate(dot(FirstPassRes[1].Sample(LinearRepeatSampler, TexCoord).xyz, -SpotLights[0].DirectionWS));
			//LightResult lit = DoLighting(FirstPassRes[4].Sample(LinearRepeatSampler, TexCoord).xyz, FirstPassRes[1].Sample(LinearRepeatSampler, TexCoord).xyz);
			if (lightInt > 0.f)
			{
				// Calculate the amount of light on this pixel.
				output.ShadedImage += (lightInt * MaterialCB.Diffuse * texColor);
				output.ShadedImage = saturate(output.ShadedImage);
			}
		}
	}
	float4 sm = FirstPassRes[4].Sample(LinearRepeatSampler, TexCoord);
	//output.ShadowMap = float4(sm.r, sm.r, sm.r, 1.f);
	output.ShadedImage = output.ShadedImage * texColor;
	return output;
}