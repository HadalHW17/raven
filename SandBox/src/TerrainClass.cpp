#include "TerrainClass.h"

TerrainClass::TerrainClass()
	:m_indexCount(0)
{

}

TerrainClass::TerrainClass(const TerrainClass& otherTerrain)
{
}

TerrainClass::~TerrainClass()
{
}

bool TerrainClass::Initialize(Raven::CommandList& commandList, char* filename)
{
	bool result;

	// Try read the heightmap data.
	if(!LoadHeightMap(filename))
	{
		return false;
	}

	NormalizeHeightmap();

	result = InitializeBuffers(commandList, filename);
	return result;
}

std::unique_ptr<TerrainClass> TerrainClass::GetTerrain(Raven::CommandList& commandList, char* filename)
{
	std::unique_ptr<TerrainClass> terrain(new TerrainClass());
	terrain->Initialize(commandList, filename);
	return terrain;
}

void TerrainClass::Render(Raven::CommandList& commandList)
{
	commandList.SetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINELIST);
	commandList.SetVertexBuffer(0, m_vertexBuffer);

	if (m_indexCount > 0)
	{
		commandList.SetIndexBuffer(m_indexBuffer);
		commandList.DrawIndexed(m_indexCount);
	}
	else
	{
		commandList.Draw(m_vertexBuffer.GetNumVertices());
	}
}

void TerrainClass::Shutdown()
{
	// Should not work.
	if (m_indexBuffer.IsValid())
	{
		m_indexBuffer.Reset();
		m_indexCount = 0;
	}
	if (m_vertexBuffer.IsValid())
	{
		m_vertexBuffer.Reset();
	}
}

bool TerrainClass::InitializeBuffers(Raven::CommandList& commandList, char* filename)
{
	using VertexCollection = std::vector<VertexData>;
	using IndexCollection = std::vector<uint16_t>;
	using float3 = DirectX::XMFLOAT3;
	using float4 = DirectX::XMFLOAT4;
	VertexCollection vertecies;
	IndexCollection indecies;
	int index = 0;
	size_t vertexCount = (m_width - 1) * (m_heigth - 1) * 12;
	m_indexCount = vertexCount;
	float positionX, positionZ;
	float3 normal = float3(0.f, 1.f, 0.f);

	// Generate the grid.
	for (int j = 0; j < m_heigth - 1; ++j)
	{
		for (int i = 0; i < m_width - 1; ++i)
		{
			size_t index1 = (m_heigth * j) + i;          // Bottom left.
			size_t index2 = (m_heigth * j) + (i + 1);      // Bottom right.
			size_t index3 = (m_heigth * (j + 1)) + i;      // Upper left.
			size_t index4 = (m_heigth * (j + 1)) + (i + 1);  // Upper right.

			// Line 1
			// Upper left.
			float3 position = float3(m_HeightMap[index3].x, m_HeightMap[index3].y, m_HeightMap[index3].z);
			vertecies.push_back({ position, normal });
			indecies.push_back(index);
			++index;

			// Upper right.
			position = float3(m_HeightMap[index4].x, m_HeightMap[index4].y, m_HeightMap[index4].z);
			vertecies.push_back({ position, normal });
			indecies.push_back(index);
			++index;

			// Line 2.
			// Upper right.
			position = float3(m_HeightMap[index4].x, m_HeightMap[index4].y, m_HeightMap[index4].z);
			vertecies.push_back({ position, normal });
			indecies.push_back(index);
			++index;

			// Bottom right.
			position = float3(m_HeightMap[index2].x, m_HeightMap[index2].y, m_HeightMap[index2].z);
			vertecies.push_back({ position, normal });
			indecies.push_back(index);
			++index;

			// Line 3.
			// Bottom right.
			position = float3(m_HeightMap[index2].x, m_HeightMap[index2].y, m_HeightMap[index2].z);
			vertecies.push_back({ position, normal });
			indecies.push_back(index);
			++index;

			// Bottom left.
			position = float3(m_HeightMap[index1].x, m_HeightMap[index1].y, m_HeightMap[index1].z);
			vertecies.push_back({ position, normal });
			indecies.push_back(index);
			++index;

			//Line 4.
			// Bottom left.
			position = float3(m_HeightMap[index1].x, m_HeightMap[index1].y, m_HeightMap[index1].z);
			vertecies.push_back({ position, normal });
			indecies.push_back(index);
			++index;

			// Upper left.
			position = float3(m_HeightMap[index3].x, m_HeightMap[index3].y, m_HeightMap[index3].z);
			vertecies.push_back({ position, normal });
			indecies.push_back(index);
			++index;
		}
	}

	// Upload vertex buffer.
	commandList.CopyVertexBuffer(m_vertexBuffer, vertecies);
	commandList.CopyIndexBuffer(m_indexBuffer, indecies);

	// Update index count.
	m_indexCount = static_cast<UINT>(indecies.size());

	return true;
}

bool TerrainClass::LoadHeightMap(char *filename)
{
	FILE* fileptr;
	int error;
	unsigned int count;
	BITMAPFILEHEADER bitmapFileHeader;
	BITMAPINFOHEADER bitmapInfoHeader;
	int imageSize, i, j, k, index;
	unsigned char* bitmapImage;
	unsigned char height;

	// Read the file in binary mode.
	error = fopen_s(&fileptr, filename, "rb");
	if (error != 0)
	{
		return false;
	}

	// Read in the file header.
	count = fread(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, fileptr);
	if (count != 1)
	{
		return false;
	}

	// Read in the bitmap info header.
	count = fread(&bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, fileptr);
	if (count != 1)
	{
		return false;
	}

	// Save the dimensions of the terrain.
	m_width = bitmapInfoHeader.biWidth;
	m_heigth = bitmapInfoHeader.biHeight;

	// Calculate the size of the bitmap image data.
	imageSize = m_width * m_heigth * 3;

	// Allocate memory for the bitmap image data.
	bitmapImage = new unsigned char[imageSize];
	if (!bitmapImage)
	{
		return false;
	}

	// Move to the beginning of the bitmap data.
	fseek(fileptr, bitmapFileHeader.bfOffBits, SEEK_SET);

	// Read in the bitmap image data.
	count = fread(bitmapImage, 1, imageSize, fileptr);
	if (count != imageSize)
	{
		return false;
	}

	// Close the file.
	error = fclose(fileptr);
	if (error != 0)
	{
		return false;
	}

	m_HeightMap = std::vector<HeightMapData>(imageSize);
	if (m_HeightMap.empty())
	{
		return false;
	}
	// Initialize the position in the image data buffer.
	k = 0;

	// Read the image data into the height map.
	for (j = 0; j < m_width; j++)
	{
		for (i = 0; i < m_heigth; i++)
		{
			height = bitmapImage[k];

			index = (m_heigth * j) + i;

			HeightMapData currentData = {i, height, j};
			m_HeightMap[index].x = (float)i;
			m_HeightMap[index].y = (float)height;
			m_HeightMap[index].z = (float)j;

			k += 3;
		}
	}

	// Release the bitmap image data.
	delete[] bitmapImage;
	bitmapImage = nullptr;

	return true;
}

void TerrainClass::NormalizeHeightmap()
{
	int i, j;


	for (j = 0; j < m_heigth; j++)
	{
		for (i = 0; i < m_width; i++)
		{
			m_HeightMap[(m_heigth * j) + i].y /= 15.0f;
		}
	}
}
