#include "SandboxUI.h"
#include "Renderer/Tonemapping.h"
#include <algorithm> // For std::clamp
#include <Engine/Application.h>
#include <Engine/Game.h>
#include <Engine/Window.h>
#include "Sandbox.h"
#include <memory>
#include <Raven/Raven.h>

void SandboxUI::OnAttach()
{
	super::OnAttach();
	
	AddOverlay(BIND_OVERLAY(SandboxUI::GUI));
}

// Helper to display a little (?) mark which shows a tooltip when hovered.
static void ShowHelpMarker(const char* desc)
{
	ImGui::TextDisabled("(?)");
	if (ImGui::IsItemHovered())
	{
		ImGui::BeginTooltip();
		ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
		ImGui::TextUnformatted(desc);
		ImGui::PopTextWrapPos();
		ImGui::EndTooltip();
	}
}

// Number of values to plot in the tonemapping curves.
static const int VALUES_COUNT = 256;
// Maximum HDR value to normalize the plot samples.
static const float HDR_MAX = 12.0f;

float LinearTonemapping(float HDR, float max)
{
	if (max > 0.0f)
	{
		return std::clamp<float>(HDR / max, 0.f, 1.f);
	}
	return HDR;
}

float LinearTonemappingPlot(void*, int index)
{
	auto game = Raven::Application::Get().GetGameRef();
	auto sandbox = dynamic_cast<Sandbox*>(game.get());
	auto g_TonemapParameters = sandbox->g_TonemapParameters;
	return LinearTonemapping(index / (float)VALUES_COUNT * HDR_MAX, g_TonemapParameters.MaxLuminance);
}

// Reinhard tone mapping.
// See: http://www.cs.utah.edu/~reinhard/cdrom/tonemap.pdf
float ReinhardTonemapping(float HDR, float k)
{
	return HDR / (HDR + k);
}

float ReinhardTonemappingPlot(void*, int index)
{
	auto game = Raven::Application::Get().GetGameRef();
	auto sandbox = dynamic_cast<Sandbox*>(game.get());
	return ReinhardTonemapping(index / (float)VALUES_COUNT * HDR_MAX, sandbox->g_TonemapParameters.K);
}

float ReinhardSqrTonemappingPlot(void*, int index)
{
	auto game = Raven::Application::Get().GetGameRef();
	auto sandbox = dynamic_cast<Sandbox*>(game.get());
	float reinhard = ReinhardTonemapping(index / (float)VALUES_COUNT * HDR_MAX, sandbox->g_TonemapParameters.K);
	return reinhard * reinhard;
}

// ACES Filmic
// See: https://www.slideshare.net/ozlael/hable-john-uncharted2-hdr-lighting/142
float ACESFilmicTonemapping(float x, float A, float B, float C, float D, float E, float F)
{
	return (((x * (A * x + C * B) + D * E) / (x * (A * x + B) + D * F)) - (E / F));
}

float ACESFilmicTonemappingPlot(void*, int index)
{
	auto game = Raven::Application::Get().GetGameRef();
	auto sandbox = dynamic_cast<Sandbox*>(game.get());
	auto g_TonemapParameters = sandbox->g_TonemapParameters;

	float HDR = index / (float)VALUES_COUNT * HDR_MAX;
	return ACESFilmicTonemapping(HDR, g_TonemapParameters.A, g_TonemapParameters.B, g_TonemapParameters.C, g_TonemapParameters.D, g_TonemapParameters.E, g_TonemapParameters.F) /
		ACESFilmicTonemapping(g_TonemapParameters.LinearWhite, g_TonemapParameters.A, g_TonemapParameters.B, g_TonemapParameters.C, g_TonemapParameters.D, g_TonemapParameters.E, g_TonemapParameters.F);
}


void SandboxUI::GUI()
{
	static bool showDemoWindow = false;
	static bool showOptions = true;

	auto game = Raven::Application::Get().GetGameRef();
	auto sandbox = dynamic_cast<Sandbox*>(game.get());

	auto &g_TonemapParameters = sandbox->g_TonemapParameters;
	auto m_pWindow = Raven::Application::Get().GetGameRef()->GetWindow();
	if (ImGui::BeginMainMenuBar())
	{
		if (ImGui::BeginMenu("File"))
		{
			if (ImGui::MenuItem("Exit", "Esc"))
			{
				Raven::Application::Get().Quit();
			}
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("View"))
		{
			ImGui::MenuItem("ImGui Demo", nullptr, &showDemoWindow);
			ImGui::MenuItem("Tonemapping", nullptr, &showOptions);

			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Options"))
		{
			bool vSync = m_pWindow->IsVSync();
			if (ImGui::MenuItem("V-Sync", "V", &vSync))
			{
				m_pWindow->SetVSync(vSync);
			}

			bool fullscreen = m_pWindow->IsFullScreen();
			if (ImGui::MenuItem("Full screen", "Alt+Enter", &fullscreen))
			{
				m_pWindow->SetFullscreen(fullscreen);
			}

			ImGui::EndMenu();
		}

		char buffer[256];

		{
			// Output a slider to scale the resolution of the HDR render target.
			float renderScale = sandbox->m_RenderScale;
			ImGui::PushItemWidth(300.0f);
			ImGui::SliderFloat("Resolution Scale", &renderScale, 0.1f, 2.0f);
			// Using Ctrl+Click on the slider, the user can set values outside of the 
			// specified range. Make sure to clamp to a sane range to avoid creating
			// giant render targets.
			renderScale = std::clamp(renderScale, 0.0f, 2.0f);

			// Output current resolution of render target.
			auto size = sandbox->GetRenderTarget().GetSize();
			ImGui::SameLine();
			sprintf_s(buffer, _countof(buffer), "(%ux%u)", size.x, size.y);
			ImGui::Text(buffer);

			// Resize HDR render target if the scale changed.
			if (renderScale != sandbox->m_RenderScale)
			{
				sandbox->m_RenderScale = renderScale;
				sandbox->RescaleHDRRenderTarget(sandbox->m_RenderScale);
			}
		}

		{
			sprintf_s(buffer, _countof(buffer), "FPS: %.2f (%.2f ms)  ", sandbox->g_FPS, 1.0 / sandbox->g_FPS * 1000.0);
			auto fpsTextSize = ImGui::CalcTextSize(buffer);
			ImGui::SameLine(ImGui::GetWindowWidth() - fpsTextSize.x);
			ImGui::Text(buffer);
		}

		ImGui::EndMainMenuBar();
	}

	if (showDemoWindow)
	{
		ImGui::ShowDemoWindow(&showDemoWindow);
	}

	if (showOptions)
	{
		ImGui::Begin("Tonemapping", &showOptions);
		{
			ImGui::TextWrapped("Use the Exposure slider to adjust the overall exposure of the HDR scene.");
			ImGui::SliderFloat("Exposure", &sandbox->g_TonemapParameters.Exposure, -10.0f, 10.0f);
			ImGui::SameLine(); ShowHelpMarker("Adjust the overall exposure of the HDR scene.");
			ImGui::SliderFloat("Gamma", &sandbox->g_TonemapParameters.Gamma, 0.01f, 5.0f);
			ImGui::SameLine(); ShowHelpMarker("Adjust the Gamma of the output image.");

			const char* toneMappingMethods[] = {
				"Linear",
				"Reinhard",
				"Reinhard Squared",
				"ACES Filmic"
			};

			ImGui::Combo("Tonemapping Methods", (int*)(&sandbox->g_TonemapParameters.TonemapMethod), toneMappingMethods, 4);

			switch (sandbox->g_TonemapParameters.TonemapMethod)
			{
			case TonemapMethod::TM_Linear:
				ImGui::PlotLines("Linear Tonemapping", &LinearTonemappingPlot, nullptr, VALUES_COUNT, 0, nullptr, 0.0f, 1.0f, ImVec2(0, 250));
				ImGui::SliderFloat("Max Brightness", &sandbox->g_TonemapParameters.MaxLuminance, 1.0f, HDR_MAX);
				ImGui::SameLine(); ShowHelpMarker("Linearly scale the HDR image by the maximum brightness.");
				break;
			case TonemapMethod::TM_Reinhard:
				ImGui::PlotLines("Reinhard Tonemapping", &ReinhardTonemappingPlot, nullptr, VALUES_COUNT, 0, nullptr, 0.0f, 1.0f, ImVec2(0, 250));
				ImGui::SliderFloat("Reinhard Constant", &sandbox->g_TonemapParameters.K, 0.01f, 10.0f);
				ImGui::SameLine(); ShowHelpMarker("The Reinhard constant is used in the denominator.");
				break;
			case TonemapMethod::TM_ReinhardSq:
				ImGui::PlotLines("Reinhard Squared Tonemapping", &ReinhardSqrTonemappingPlot, nullptr, VALUES_COUNT, 0, nullptr, 0.0f, 1.0f, ImVec2(0, 250));
				ImGui::SliderFloat("Reinhard Constant", &sandbox->g_TonemapParameters.K, 0.01f, 10.0f);
				ImGui::SameLine(); ShowHelpMarker("The Reinhard constant is used in the denominator.");
				break;
			case TonemapMethod::TM_ACESFilmic:
				ImGui::PlotLines("ACES Filmic Tonemapping", &ACESFilmicTonemappingPlot, nullptr, VALUES_COUNT, 0, nullptr, 0.0f, 1.0f, ImVec2(0, 250));
				ImGui::SliderFloat("Shoulder Strength", &g_TonemapParameters.A, 0.01f, 5.0f);
				ImGui::SliderFloat("Linear Strength", &g_TonemapParameters.B, 0.0f, 100.0f);
				ImGui::SliderFloat("Linear Angle", &g_TonemapParameters.C, 0.0f, 1.0f);
				ImGui::SliderFloat("Toe Strength", &g_TonemapParameters.D, 0.01f, 1.0f);
				ImGui::SliderFloat("Toe Numerator", &g_TonemapParameters.E, 0.0f, 10.0f);
				ImGui::SliderFloat("Toe Denominator", &g_TonemapParameters.F, 1.0f, 10.0f);
				ImGui::SliderFloat("Linear White", &g_TonemapParameters.LinearWhite, 1.0f, 120.0f);
				break;
			default:
				break;
			}

			const char* renderTexture[] = {
				"Albedo",
				"Normals",
				"Diffuse",
				"Specular",
				"Position",
				"Shaded"
			};

			ImGui::Combo("Present texture", (int*)(&sandbox->g_finalTexture), renderTexture, 6);

		}

		if (ImGui::Button("Reset to Defaults"))
		{
			TonemapMethod method = sandbox->g_TonemapParameters.TonemapMethod;
			sandbox->g_TonemapParameters = TonemapParameters();
			sandbox->g_TonemapParameters.TonemapMethod = method;
		}



		ImGui::End();
	}
}
