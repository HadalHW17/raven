#pragma once
#include "stdio.h"
#include <string>
#include <functional>


#ifdef RE_PLATFORM_WINDOWS
#ifdef RE_BUILD_DLL
#define RAVEN_API __declspec(dllexport)
#else
#define	RAVEN_API __declspec(dllimport)
#endif	
#else
#error Ray Engine currently only supports Windows!
#endif



#define BIT(x) (1 << x)
#define sphere_tracing

#define K_INFINITY	1e20f					// Mathematical infinity
#define K_EPSILON	1e-4f					// Error value
#define M_PI		3.14159265358979323846  // pi
#define M_PI_2		1.57079632679489661923  // pi/2

// settings
#define SCR_WIDTH 500
#define SCR_HEIGHT 500

// testing/tracing function used pervasively in tests.  if the condition is unsatisfied
// then spew and fail the function immediately (doing no cleanup)
#define AssertOrQuit(x) \
    if (!(x)) \
    { \
        fprintf(stdout, "Assert unsatisfied in %s at %s:%d\n", __FUNCTION__, __FILE__, __LINE__); \
        return 1; \
    }


#ifndef RAY_ENGINE_ASSERT
#include <cassert>
#define RAY_ENGINE_ASSERT(condition, msg) {assert((msg, condition));}
#endif // !RAY_ENGINE_ASSERT


#ifdef RE_DEVELOPMENT
#define RE_LOG(msg) std::cout << msg << std::endl;
#define REE_LOG(msg) std::cout << "!!!" << msg << "!!!" << std::endl;
#else 
#define RE_LOG(msg)
#define REE_LOG(msg)
#endif

#ifndef BIND_EVENT_FN(x)
#define BIND_EVENT_FN(x) std::bind(&x, this, std::placeholders::_1)
#endif

#ifndef BIND_OVERLAY(x)
#define BIND_OVERLAY(x) std::bind(&x, this)
#endif

