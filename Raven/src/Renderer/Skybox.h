#pragma once

#include "Raven/Raven.h"
#include <memory>
#include "Texture.h"
#include "RootSignature.h"
#include <d3d12.h>
#include <DirectXMath.h>
#include "CommandList.h"
#include "RenderTarget.h"

namespace Raven
{
	class Mesh;
	class Camera;

	class RAVEN_API Skybox
	{
	public:
		Skybox();

		void Load(CommandList& commandList, const std::wstring texturePath, const RenderTarget& renderTo);

		void Render(CommandList& commandList,const Camera& camera);

	private:
		// Skybox root signature
		Raven::RootSignature m_SkyboxSignature;

		// Skybox pipeline state.
		Microsoft::WRL::ComPtr<ID3D12PipelineState> m_SkyboxPipelineState;

		std::unique_ptr<Mesh> m_SkyboxMesh;

		Texture m_SkyboxTexture;
		Texture m_SkyboxCubemap;
	};
}