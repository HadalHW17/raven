#pragma once

#include <DirectXMath.h>
#include "Engine/Application.h"
#include "Engine/Game.h"

struct PointLight
{
	PointLight()
		: PositionWS(0.0f, 0.0f, 0.0f, 1.0f)
		, PositionVS(0.0f, 0.0f, 0.0f, 1.0f)
		, Color(1.0f, 1.0f, 1.0f, 1.0f)
		, Intensity(1.0f)
		, Attenuation(0.0f)
	{}

	DirectX::XMFLOAT4    PositionWS; // Light position in world space.
	//----------------------------------- (16 byte boundary)
	DirectX::XMFLOAT4    PositionVS; // Light position in view space.
	//----------------------------------- (16 byte boundary)
	DirectX::XMFLOAT4    Color;
	//----------------------------------- (16 byte boundary)
	float       Intensity;
	float       Attenuation;
	float       Padding[2];             // Pad to 16 bytes.
	//----------------------------------- (16 byte boundary)
	// Total:                              16 * 4 = 64 bytes
};

struct SpotLight
{
	SpotLight()
		: PositionWS(0.0f, 0.0f, 0.0f, 1.0f)
		, PositionVS(0.0f, 0.0f, 0.0f, 1.0f)
		, DirectionWS(0.0f, 0.0f, 1.0f, 0.0f)
		, DirectionVS(0.0f, 0.0f, 1.0f, 0.0f)
		, Color(1.0f, 1.0f, 1.0f, 1.0f)
		, Intensity(1.0f)
		, SpotAngle(DirectX::XM_PIDIV2)
		, Attenuation(0.0f)
	{}

	DirectX::XMFLOAT4    PositionWS; // Light position in world space.
	//----------------------------------- (16 byte boundary)
	DirectX::XMFLOAT4    PositionVS; // Light position in view space.
	//----------------------------------- (16 byte boundary)
	DirectX::XMFLOAT4    DirectionWS; // Light direction in world space.
	//----------------------------------- (16 byte boundary)
	DirectX::XMFLOAT4    DirectionVS; // Light direction in view space.
	//----------------------------------- (16 byte boundary)
	DirectX::XMFLOAT4    Color;
	//----------------------------------- (16 byte boundary)
	float       Intensity;
	float       SpotAngle;
	float       Attenuation;
	float       Padding;                // Pad to 16 bytes.
	//----------------------------------- (16 byte boundary)
	// Total:                              16 * 6 = 96 bytes

	DirectX::XMMATRIX MakeViewMatrix()
	{
		DirectX::XMVECTOR up = DirectX::XMVectorSet(0.f, 1.f, 0.f, 1.f);
		DirectX::XMVECTOR dir = DirectX::XMVectorSet(DirectionWS.x, DirectionWS.y, DirectionWS.z, 1.f);
		DirectX::XMVECTOR pos = DirectX::XMVectorSet(PositionWS.x, PositionWS.y, PositionWS.z, 1.f);
		return DirectX::XMMatrixLookAtLH(pos, dir, up);
	}

	DirectX::XMMATRIX MakeProjectionMatrix(float tNear, float tFar)
	{
		float fieldOfView, screenAspect;
		int h = Raven::Application::Get().GetGameRef()->GetClientHeight();
		int w = Raven::Application::Get().GetGameRef()->GetClientWidth();
		// Setup field of view and screen aspect for a square light source.
		fieldOfView = (float)45.f;
		screenAspect = w / (float)h;

		return DirectX::XMMatrixPerspectiveFovLH(fieldOfView, screenAspect, tNear, tFar);
	}
};

struct DirectionalLight
{
	DirectionalLight()
		: DirectionWS(0.0f, 0.0f, 1.0f, 0.0f)
		, DirectionVS(0.0f, 0.0f, 1.0f, 0.0f)
		, Color(1.0f, 1.0f, 1.0f, 1.0f)
	{}

	DirectX::XMFLOAT4    DirectionWS; // Light direction in world space.
	//----------------------------------- (16 byte boundary)
	DirectX::XMFLOAT4    DirectionVS; // Light direction in view space.
	//----------------------------------- (16 byte boundary)
	DirectX::XMFLOAT4    Color;
	//----------------------------------- (16 byte boundary)
	// Total:                              16 * 3 = 48 bytes 
};
