#pragma once


#include "CommandList.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"

#include <DirectXMath.h>
#include <d3d12.h>

#include <wrl.h>

#include <memory> // For std::unique_ptr
#include <vector>
#include "Raven/Raven.h"
#include "Renderer/Material.h"

#include "Renderer/Texture.h"

struct aiMesh;
namespace Raven
{
	// Vertex struct holding position, normal vector, and texture mapping information.
	struct RAVEN_API VertexPositionNormalTexture
	{
		VertexPositionNormalTexture()
		{ }

		VertexPositionNormalTexture(const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT3& normal, const DirectX::XMFLOAT2& textureCoordinate)
			: position(position),
			normal(normal),
			textureCoordinate(textureCoordinate)
		{ }

		VertexPositionNormalTexture(DirectX::FXMVECTOR position, DirectX::FXMVECTOR normal, DirectX::FXMVECTOR textureCoordinate)
		{
			XMStoreFloat3(&this->position, position);
			XMStoreFloat3(&this->normal, normal);
			XMStoreFloat2(&this->textureCoordinate, textureCoordinate);
		}

		DirectX::XMFLOAT3 position;
		DirectX::XMFLOAT3 normal;
		DirectX::XMFLOAT2 textureCoordinate;
		DirectX::XMFLOAT3 binormal;
		DirectX::XMFLOAT3 tangent;

		static const int InputElementCount = 5;
		static const D3D12_INPUT_ELEMENT_DESC InputElements[InputElementCount];
	};

	using VertexCollection = std::vector<VertexPositionNormalTexture>;
	using IndexCollection = std::vector<uint16_t>;

	class RAVEN_API Mesh
	{
	public:

		void Render(CommandList& commandList, uint32_t instanceCount = 1, uint32_t firstInstance = 0);

		static std::unique_ptr<Mesh> CreateCube(CommandList& commandList, float size = 1, bool rhcoords = false);
		static std::unique_ptr<Mesh> CreateSphere(CommandList& commandList, float diameter = 1, size_t tessellation = 16, bool rhcoords = false);
		static std::unique_ptr<Mesh> CreateCone(CommandList& commandList, float diameter = 1, float height = 1, size_t tessellation = 32, bool rhcoords = false);
		static std::unique_ptr<Mesh> CreateTorus(CommandList& commandList, float diameter = 1, float thickness = 0.333f, size_t tessellation = 32, bool rhcoords = false);
		static std::unique_ptr<Mesh> CreatePlane(CommandList& commandList, float width = 1, float height = 1, bool rhcoords = false);
		static std::unique_ptr<Mesh> LoadFromFIle(CommandList& commandList, const char* filepath, bool rhCoords);
		static std::unique_ptr<Mesh> ConvertFromAssimp(CommandList& commandList, const aiMesh* mesh, bool rhCoords, const Texture& texture, const Texture& normalMap);

		inline Texture& GetTexture() { return m_Texture; }
		inline Texture& GetNormalMap() { return m_NormalMap; }
	protected:

	private:
		friend struct std::default_delete<Mesh>;

		Mesh();
		Mesh(const Mesh& copy) = delete;
		Mesh& operator = (const Mesh&) = delete;
		virtual ~Mesh();

		void Initialize(CommandList& commandList, VertexCollection& vertices, IndexCollection& indices, bool rhcoords);

		VertexBuffer m_VertexBuffer;
		IndexBuffer m_IndexBuffer;

		UINT m_IndexCount;

		Material m_Material;
		Texture m_Texture;
		Texture m_NormalMap;
	};
}