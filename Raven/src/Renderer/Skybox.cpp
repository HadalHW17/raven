#include "rvpch.h"

#include "Skybox.h"
#include "Mesh.h"
#include "CommandList.h"
#include "Engine/Application.h"
#include "CommandQueue.h"
#include "RenderTarget.h"
#include "Camera.h"
#include "Engine/Helpers.h"
#include "Engine/d3dx12.h"

#include <wrl.h>
using namespace Microsoft::WRL;

#include <DirectXMath.h>
using namespace DirectX;


namespace Raven
{
	Skybox::Skybox()
	{
	}

	void Skybox::Load(CommandList& commandList, const std::wstring texturePath, const RenderTarget& renderTo)
	{
		auto device = Application::Get().GetDevice();

		// Create mesh for the skybox.
		m_SkyboxMesh = Mesh::CreateCube(commandList, 1.0f, true);

		// Load panorama texture
		commandList.LoadTextureFromFile(m_SkyboxTexture, texturePath);

		// Create a cubemap for the HDR panorama.
		auto cubemapDesc = m_SkyboxTexture.GetD3D12ResourceDesc();
		cubemapDesc.Width = cubemapDesc.Height = 1024;
		cubemapDesc.DepthOrArraySize = 6;
		cubemapDesc.MipLevels = 0;

		m_SkyboxCubemap = Raven::Texture(cubemapDesc, nullptr, Raven::TextureUsage::Albedo, L"Grace Cathedral Cubemap");
		// Convert the 2D panorama to a 3D cubemap.
		commandList.PanoToCubemap(m_SkyboxCubemap, m_SkyboxTexture);

		// Create a root signature and PSO for the skybox shaders.
		{
			D3D12_FEATURE_DATA_ROOT_SIGNATURE featureData = {};
			featureData.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_1;
			if (FAILED(device->CheckFeatureSupport(D3D12_FEATURE_ROOT_SIGNATURE, &featureData, sizeof(featureData))))
			{
				featureData.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_0;
			}

			// Load the Skybox shaders.
			ComPtr<ID3DBlob> vs;
			ComPtr<ID3DBlob> ps;
			ThrowIfFailed(D3DReadFileToBlob(L"data/shaders/Sandbox/Skybox_VS.cso", &vs));
			ThrowIfFailed(D3DReadFileToBlob(L"data/shaders/Sandbox/Skybox_PS.cso", &ps));

			// Setup the input layout for the skybox vertex shader.
			D3D12_INPUT_ELEMENT_DESC inputLayout[1] = {
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			};

			// Allow input layout and deny unnecessary access to certain pipeline stages.
			D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
				D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS;

			CD3DX12_DESCRIPTOR_RANGE1 descriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);

			CD3DX12_ROOT_PARAMETER1 rootParameters[2];
			rootParameters[0].InitAsConstants(sizeof(DirectX::XMMATRIX) / 4, 0, 0, D3D12_SHADER_VISIBILITY_VERTEX);
			rootParameters[1].InitAsDescriptorTable(1, &descriptorRange, D3D12_SHADER_VISIBILITY_PIXEL);

			CD3DX12_STATIC_SAMPLER_DESC linearClampSampler(0, D3D12_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR, D3D12_TEXTURE_ADDRESS_MODE_CLAMP, D3D12_TEXTURE_ADDRESS_MODE_CLAMP, D3D12_TEXTURE_ADDRESS_MODE_CLAMP);

			CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDescription;
			rootSignatureDescription.Init_1_1(2, rootParameters, 1, &linearClampSampler, rootSignatureFlags);

			m_SkyboxSignature.SetRootSignatureDesc(rootSignatureDescription.Desc_1_1, featureData.HighestVersion);

			// Setup the Skybox pipeline state.
			struct SkyboxPipelineState
			{
				CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE pRootSignature;
				CD3DX12_PIPELINE_STATE_STREAM_INPUT_LAYOUT InputLayout;
				CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
				CD3DX12_PIPELINE_STATE_STREAM_VS VS;
				CD3DX12_PIPELINE_STATE_STREAM_PS PS;
				CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
			} skyboxPipelineStateStream;

			skyboxPipelineStateStream.pRootSignature = m_SkyboxSignature.GetRootSignature().Get();
			skyboxPipelineStateStream.InputLayout = { inputLayout, 1 };
			skyboxPipelineStateStream.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
			skyboxPipelineStateStream.VS = CD3DX12_SHADER_BYTECODE(vs.Get());
			skyboxPipelineStateStream.PS = CD3DX12_SHADER_BYTECODE(ps.Get());
			skyboxPipelineStateStream.RTVFormats = renderTo.GetRenderTargetFormats();

			D3D12_PIPELINE_STATE_STREAM_DESC skyboxPipelineStateStreamDesc = {
				sizeof(SkyboxPipelineState), &skyboxPipelineStateStream
			};
			ThrowIfFailed(device->CreatePipelineState(&skyboxPipelineStateStreamDesc, IID_PPV_ARGS(&m_SkyboxPipelineState)));
		}
	}

	void Skybox::Render(CommandList& commandList, const Camera& camera)
	{

		// Render the skybox.

		// The view matrix should only consider the camera's rotation, but not the translation.
		auto viewMatrix = XMMatrixTranspose(XMMatrixRotationQuaternion(camera.get_Rotation()));
		auto projMatrix = camera.get_ProjectionMatrix();
		auto viewProjMatrix = viewMatrix * projMatrix;

		commandList.SetPipelineState(m_SkyboxPipelineState);
		commandList.SetGraphicsRootSignature(m_SkyboxSignature);

		commandList.SetGraphics32BitConstants(0, viewProjMatrix);

		D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
		srvDesc.Format = m_SkyboxCubemap.GetD3D12ResourceDesc().Format;
		srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURECUBE;
		srvDesc.TextureCube.MipLevels = (UINT)-1; // Use all mips.

		// TODO: Need a better way to bind a cubemap.
		commandList.SetShaderResourceView(1, 0, m_SkyboxCubemap, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, 0, D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES, &srvDesc);

		// Send a render command to a command list.
		m_SkyboxMesh->Render(commandList);
	}
}