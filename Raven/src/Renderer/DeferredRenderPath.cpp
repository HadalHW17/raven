#include "rvpch.h"

#include "DeferredRenderPath.h"

#include "Engine/Application.h"
#include "Engine/Game.h"
#include "Engine/Helpers.h"
#include "Renderer/Mesh.h"
#include "Engine/Window.h"
#include "Renderer/Tonemapping.h"

#include <wrl.h>
using namespace Microsoft::WRL;

#include <Engine/d3dx12.h>
#include <d3dcompiler.h>
#include <DirectXColors.h>
#include <DirectXMath.h>
#include <algorithm> // For std::clamp

using namespace DirectX;

#include <algorithm> // For std::min and std::max.
#if defined(min)
#undef min
#endif

#if defined(max)
#undef max
#endif


void Raven::DeferredRenderPath::SetUpRenderTarget(CommandList& commandList)
{
	m_ScissorRect = CD3DX12_RECT(0, 0, LONG_MAX, LONG_MAX);
	// Get desired dimensions for our render target.
	int rtWidth = Application::Get().GetGameRef()->GetClientWidth();
	int rtHeigth = Application::Get().GetGameRef()->GetClientHeight();

	// Create an intermediate render target format.
	DXGI_FORMAT HDRFormat = DXGI_FORMAT_R16G16B16A16_FLOAT;
	DXGI_FORMAT LightDepth = DXGI_FORMAT_R32G32B32A32_FLOAT;

	// Create an intermediate render target format for normal.
	DXGI_FORMAT NormalFormat = DXGI_FORMAT_R16G16B16A16_UNORM;

	// Create an intermediate depth buffer format.
	DXGI_FORMAT depthBufferFormat = DXGI_FORMAT_D32_FLOAT;

	// Create an off-screen render target with a single color buffer and a depth buffer.
	auto colorDesc = CD3DX12_RESOURCE_DESC::Tex2D(HDRFormat, rtWidth, rtHeigth);
	colorDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;

	// Create an off-screen render target with a single color buffer and a depth buffer.
	auto LightDepthColorDesc = CD3DX12_RESOURCE_DESC::Tex2D(LightDepth, rtWidth, rtHeigth);
	LightDepthColorDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;

	// Create colour description for normal texture.
	auto normalColorDesc = CD3DX12_RESOURCE_DESC::Tex2D(NormalFormat, rtWidth, rtHeigth);
	normalColorDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;

	// Create description for depth buffer.
	auto depthDesc = CD3DX12_RESOURCE_DESC::Tex2D(depthBufferFormat, rtWidth, rtHeigth);
	depthDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	// Clear colour value for colour textures.
	D3D12_CLEAR_VALUE colorClearValue;
	colorClearValue.Format = colorDesc.Format;
	colorClearValue.Color[0] = 0.4f;
	colorClearValue.Color[1] = 0.6f;
	colorClearValue.Color[2] = 0.9f;
	colorClearValue.Color[3] = 1.0f;

	// Clear colour value for colour textures.
	D3D12_CLEAR_VALUE lightDepthColorClearValue;
	lightDepthColorClearValue.Format = LightDepthColorDesc.Format;
	lightDepthColorClearValue.Color[0] = 0.4f;
	lightDepthColorClearValue.Color[1] = 0.6f;
	lightDepthColorClearValue.Color[2] = 0.9f;
	lightDepthColorClearValue.Color[3] = 1.0f;

	// Clear value for the depth buffer.
	D3D12_CLEAR_VALUE depthClearValue;
	depthClearValue.Format = depthDesc.Format;
	depthClearValue.DepthStencil = { 1.0f, 0 };


	// Texture for colour data of intersected scene.
	Raven::Texture AlbedoTexture = Raven::Texture(colorDesc, &colorClearValue,
		Raven::TextureUsage::RenderTarget,
		L"Albedo Texture");

	// Bind normal texture output for deferred rendering.
	Raven::Texture NormalTexture = Raven::Texture(colorDesc, &colorClearValue,
		Raven::TextureUsage::RenderTarget, L"Normal Texture");

	// Bind diffuse texture output for deferred rendering.
	Raven::Texture DiffuseTexture = Raven::Texture(colorDesc, &colorClearValue,
		Raven::TextureUsage::RenderTarget, L"Diffuse Texture");

	// Bind specular texture output for deferred rendering.
	Raven::Texture LightDepthTexture = Raven::Texture(LightDepthColorDesc, &lightDepthColorClearValue,
		Raven::TextureUsage::RenderTarget, L"Specular Texture");

	// Bind position texture output for deferred rendering.
	Raven::Texture PositionTexture = Raven::Texture(colorDesc, &colorClearValue,
		Raven::TextureUsage::RenderTarget, L"Position Texture");

	// Bind final shaded texture output for deferred rendering.
	Raven::Texture HDRShadedTexture = Raven::Texture(colorDesc, &colorClearValue,
		Raven::TextureUsage::RenderTarget, L"Shaded Texture");

	// Create a depth buffer for the deferred rendering render target.
	Raven::Texture depthTexture = Raven::Texture(depthDesc, &depthClearValue,
		Raven::TextureUsage::Depth,
		L"Depth Render Target");

	// Attach textures created above to the render target.
	m_DeferredRenderTarget.AttachTexture(Raven::AttachmentPoint::Color0, AlbedoTexture);
	m_DeferredRenderTarget.AttachTexture(Raven::AttachmentPoint::Color1, NormalTexture);
	m_DeferredRenderTarget.AttachTexture(Raven::AttachmentPoint::Color2, DiffuseTexture);
	m_DeferredRenderTarget.AttachTexture(Raven::AttachmentPoint::Color3, LightDepthTexture);
	m_DeferredRenderTarget.AttachTexture(Raven::AttachmentPoint::Color4, PositionTexture);
	m_DeferredRenderTarget.AttachTexture(Raven::AttachmentPoint::Color5, HDRShadedTexture);
	m_DeferredRenderTarget.AttachTexture(Raven::AttachmentPoint::DepthStencil, depthTexture);

}


void Raven::DeferredRenderPath::InitPSO(CommandList& commandList)
{
	m_Skybox.Load(commandList, L"Assets/Textures/kiara_1_dawn_2k.hdr", GetRenderTarget());

	auto device = Raven::Application::Get().GetDevice();

	D3D12_FEATURE_DATA_ROOT_SIGNATURE featureData = {};
	featureData.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_1;
	if (FAILED(device->CheckFeatureSupport(D3D12_FEATURE_ROOT_SIGNATURE, &featureData, sizeof(featureData))))
	{
		featureData.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_0;
	}

	// Set up shader for generating depth texture.
	m_DepthShader = std::make_shared<Raven::DepthShader>();
	m_DepthShader->InitializeShader(commandList);

	// Create a root signature and PSO for the first render pass.
	{
		// Load the HDR shaders.
		ComPtr<ID3DBlob> vs;
		ComPtr<ID3DBlob> ps;
		ThrowIfFailed(D3DReadFileToBlob(L"data/shaders/Sandbox/HDR_VS.cso", &vs));
		ThrowIfFailed(D3DReadFileToBlob(L"data/shaders/Sandbox/DefferedFirstPass_PS.cso", &ps));

		// Allow input layout and deny unnecessary access to certain pipeline stages.
		D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
			D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS;

		CD3DX12_DESCRIPTOR_RANGE1 descriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 2, 2);

		CD3DX12_RASTERIZER_DESC rasterizerDesc(D3D12_DEFAULT);
		rasterizerDesc.CullMode = D3D12_CULL_MODE_NONE;

		CD3DX12_ROOT_PARAMETER1 rootParameters[4];
		// Matrices into the constant buffer.
		rootParameters[0].InitAsConstantBufferView(0, 0, D3D12_ROOT_DESCRIPTOR_FLAG_NONE, D3D12_SHADER_VISIBILITY_VERTEX); 
		// Material data into the constant buffer.
		rootParameters[1].InitAsConstantBufferView(0, 1, D3D12_ROOT_DESCRIPTOR_FLAG_NONE, D3D12_SHADER_VISIBILITY_PIXEL);
		// Texture into the descriptor table.
		rootParameters[2].InitAsDescriptorTable(1, &descriptorRange, D3D12_SHADER_VISIBILITY_PIXEL);
		rootParameters[3].InitAsConstants(sizeof(XMFLOAT4),1,0, D3D12_SHADER_VISIBILITY_VERTEX);

		// Prepare linear sampler for the colour texture.
		CD3DX12_STATIC_SAMPLER_DESC linearRepeatSampler(0, D3D12_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR);

		// Initialize root signature.
		CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDescription;
		rootSignatureDescription.Init_1_1(4, rootParameters, 1, &linearRepeatSampler, rootSignatureFlags);
		m_FirstPassRootSignature.SetRootSignatureDesc(rootSignatureDescription.Desc_1_1, featureData.HighestVersion);

		// Setup the first pass pipeline state.
		struct HDRPipelineStateStream
		{
			CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE pRootSignature;
			CD3DX12_PIPELINE_STATE_STREAM_INPUT_LAYOUT InputLayout;
			CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
			CD3DX12_PIPELINE_STATE_STREAM_VS VS;
			CD3DX12_PIPELINE_STATE_STREAM_PS PS;
			CD3DX12_PIPELINE_STATE_STREAM_RASTERIZER Rasterizer;
			CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL_FORMAT DSVFormat;
			CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
		} firstPassPipelineStateStream;

		firstPassPipelineStateStream.pRootSignature = m_FirstPassRootSignature.GetRootSignature().Get();
		firstPassPipelineStateStream.InputLayout = { VertexPositionNormalTexture::InputElements, VertexPositionNormalTexture::InputElementCount };
		firstPassPipelineStateStream.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		firstPassPipelineStateStream.VS = CD3DX12_SHADER_BYTECODE(vs.Get());
		firstPassPipelineStateStream.PS = CD3DX12_SHADER_BYTECODE(ps.Get());
		firstPassPipelineStateStream.Rasterizer = rasterizerDesc;
		firstPassPipelineStateStream.DSVFormat = m_DeferredRenderTarget.GetDepthStencilFormat();
		firstPassPipelineStateStream.RTVFormats = m_DeferredRenderTarget.GetRenderTargetFormats();

		D3D12_PIPELINE_STATE_STREAM_DESC firstPassPipelineStateStreamDesc = {
			sizeof(HDRPipelineStateStream), & firstPassPipelineStateStream
		};

		// Create the pipeline state
		ThrowIfFailed(device->CreatePipelineState(&firstPassPipelineStateStreamDesc , IID_PPV_ARGS(&m_FirstPassPSO)));
	}

	// Create root signature for second pass of deferred shading.
	{
		// Load the shaders.
		ComPtr<ID3DBlob> vs;
		ComPtr<ID3DBlob> ps;
		ThrowIfFailed(D3DReadFileToBlob(L"data/shaders/Sandbox/HDRtoSDR_VS.cso", &vs)); // This shader just maps a quad to the screen.
		ThrowIfFailed(D3DReadFileToBlob(L"data/shaders/Sandbox/DefferedSecondPass_PS.cso", &ps));

		// Allow input layout and deny unnecessary access to certain pipeline stages.
		D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
			D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS;

		CD3DX12_DESCRIPTOR_RANGE1 descriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 5, 2);

		CD3DX12_ROOT_PARAMETER1 rootParameters[RootParameters::NumRootParameters];
		rootParameters[RootParameters::MatricesCB].InitAsConstantBufferView(0, 0, D3D12_ROOT_DESCRIPTOR_FLAG_NONE, D3D12_SHADER_VISIBILITY_VERTEX);
		rootParameters[RootParameters::MaterialCB].InitAsConstantBufferView(0, 1, D3D12_ROOT_DESCRIPTOR_FLAG_NONE, D3D12_SHADER_VISIBILITY_PIXEL);
		rootParameters[RootParameters::LightPropertiesCB].InitAsConstants(sizeof(LightProperties) / 4, 1, 0, D3D12_SHADER_VISIBILITY_PIXEL);
		rootParameters[RootParameters::PointLights].InitAsShaderResourceView(0, 0, D3D12_ROOT_DESCRIPTOR_FLAG_NONE, D3D12_SHADER_VISIBILITY_PIXEL);
		rootParameters[RootParameters::SpotLights].InitAsShaderResourceView(1, 0, D3D12_ROOT_DESCRIPTOR_FLAG_NONE, D3D12_SHADER_VISIBILITY_PIXEL);
		rootParameters[RootParameters::Textures].InitAsDescriptorTable(1, &descriptorRange, D3D12_SHADER_VISIBILITY_PIXEL);

		// Prepare texture sampler.
		CD3DX12_STATIC_SAMPLER_DESC linearRepeatSampler(0, D3D12_FILTER_MIN_MAG_MIP_LINEAR, D3D12_TEXTURE_ADDRESS_MODE_CLAMP, D3D12_TEXTURE_ADDRESS_MODE_CLAMP, D3D12_TEXTURE_ADDRESS_MODE_CLAMP);

		CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDescription;
		rootSignatureDescription.Init_1_1(RootParameters::NumRootParameters, rootParameters, 1, &linearRepeatSampler, rootSignatureFlags);

		m_ShadeRootSignature.SetRootSignatureDesc(rootSignatureDescription.Desc_1_1, featureData.HighestVersion);

		// Setup the shading pipeline state.
		// No need for the depth buffer, since we are only mapping a quad to a screen.
		struct HDRPipelineStateStream
		{
			CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE pRootSignature;
			CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
			CD3DX12_PIPELINE_STATE_STREAM_VS VS;
			CD3DX12_PIPELINE_STATE_STREAM_PS PS;
			CD3DX12_PIPELINE_STATE_STREAM_RASTERIZER Rasterizer;
			CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
		} shaderPipelineStateStream;
		CD3DX12_RASTERIZER_DESC rasterizerDesc(D3D12_DEFAULT);
		rasterizerDesc.CullMode = D3D12_CULL_MODE_NONE;

		shaderPipelineStateStream.pRootSignature = m_ShadeRootSignature.GetRootSignature().Get();
		shaderPipelineStateStream.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		shaderPipelineStateStream.VS = CD3DX12_SHADER_BYTECODE(vs.Get());
		shaderPipelineStateStream.PS = CD3DX12_SHADER_BYTECODE(ps.Get());
		shaderPipelineStateStream.Rasterizer = rasterizerDesc;
		shaderPipelineStateStream.RTVFormats = m_DeferredRenderTarget.GetRenderTargetFormats();

		D3D12_PIPELINE_STATE_STREAM_DESC shaderPipelineStateStreamDesc = {
			sizeof(HDRPipelineStateStream), &shaderPipelineStateStream
		};
		ThrowIfFailed(device->CreatePipelineState(&shaderPipelineStateStreamDesc, IID_PPV_ARGS(&m_ShadePSO)));
	}

	// Create the SDR Root Signature
	{
		auto window = Application::Get().GetGameRef()->GetWindow();
		CD3DX12_DESCRIPTOR_RANGE1 descriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);

		CD3DX12_ROOT_PARAMETER1 rootParameters[2];
		rootParameters[0].InitAsConstants(sizeof(TonemapParameters) / 4, 0, 0, D3D12_SHADER_VISIBILITY_PIXEL);
		rootParameters[1].InitAsDescriptorTable(1, &descriptorRange, D3D12_SHADER_VISIBILITY_PIXEL);

		CD3DX12_STATIC_SAMPLER_DESC linearClampsSampler(0, D3D12_FILTER_COMPARISON_MIN_MAG_MIP_POINT, D3D12_TEXTURE_ADDRESS_MODE_CLAMP, D3D12_TEXTURE_ADDRESS_MODE_CLAMP, D3D12_TEXTURE_ADDRESS_MODE_CLAMP);

		CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDescription;
		rootSignatureDescription.Init_1_1(2, rootParameters, 1, &linearClampsSampler);

		m_PostProcessRootSignature.SetRootSignatureDesc(rootSignatureDescription.Desc_1_1, featureData.HighestVersion);

		// Create the SDR PSO
		ComPtr<ID3DBlob> vs;
		ComPtr<ID3DBlob> ps;
		ThrowIfFailed(D3DReadFileToBlob(L"data/shaders/Sandbox/HDRtoSDR_VS.cso", &vs));
		ThrowIfFailed(D3DReadFileToBlob(L"data/shaders/Sandbox/HDRtoSDR_PS.cso", &ps));

		CD3DX12_RASTERIZER_DESC rasterizerDesc(D3D12_DEFAULT);
		rasterizerDesc.CullMode = D3D12_CULL_MODE_NONE;

		struct SDRPipelineStateStream
		{
			CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE pRootSignature;
			CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
			CD3DX12_PIPELINE_STATE_STREAM_VS VS;
			CD3DX12_PIPELINE_STATE_STREAM_PS PS;
			CD3DX12_PIPELINE_STATE_STREAM_RASTERIZER Rasterizer;
			CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
		} sdrPipelineStateStream;

		sdrPipelineStateStream.pRootSignature = m_PostProcessRootSignature.GetRootSignature().Get();
		sdrPipelineStateStream.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		sdrPipelineStateStream.VS = CD3DX12_SHADER_BYTECODE(vs.Get());
		sdrPipelineStateStream.PS = CD3DX12_SHADER_BYTECODE(ps.Get());
		sdrPipelineStateStream.Rasterizer = rasterizerDesc;
		sdrPipelineStateStream.RTVFormats = window->GetRenderTarget().GetRenderTargetFormats();

		D3D12_PIPELINE_STATE_STREAM_DESC sdrPipelineStateStreamDesc = {
			sizeof(SDRPipelineStateStream), &sdrPipelineStateStream
		};
		ThrowIfFailed(device->CreatePipelineState(&sdrPipelineStateStreamDesc, IID_PPV_ARGS(&m_PostProcessPSO)));
	}
}

void Raven::DeferredRenderPath::ClearRenderTarget(CommandList& commandList)
{
	// Clear the render targets.
	{
		FLOAT clearColor[] = { 0.4f, 0.6f, 0.9f, 1.0f };

		commandList.ClearTexture(m_DeferredRenderTarget.GetTexture(Raven::AttachmentPoint::Color0), clearColor);
		commandList.ClearTexture(m_DeferredRenderTarget.GetTexture(Raven::AttachmentPoint::Color1), clearColor);
		commandList.ClearTexture(m_DeferredRenderTarget.GetTexture(Raven::AttachmentPoint::Color2), clearColor);
		commandList.ClearTexture(m_DeferredRenderTarget.GetTexture(Raven::AttachmentPoint::Color3), clearColor);
		commandList.ClearTexture(m_DeferredRenderTarget.GetTexture(Raven::AttachmentPoint::Color4), clearColor);
		commandList.ClearDepthStencilTexture(m_DeferredRenderTarget.GetTexture(Raven::AttachmentPoint::DepthStencil), D3D12_CLEAR_FLAG_DEPTH);

		m_DepthShader->ClearRenderTarget(commandList);
	}
}

void Raven::DeferredRenderPath::PreRender(CommandList& commandList, const Camera& camera, std::vector<SpotLight>& m_SpotLights, std::vector<SceneObject*>& sceneObjects)
{
	// Generate depth texture for shadow map.
	m_DepthShader->SetShaderParameters(commandList, sceneObjects[0]->GetWorldMatrix(), m_SpotLights[0].MakeViewMatrix(), m_SpotLights[0].MakeProjectionMatrix(0.1f, 100.f));
	m_DepthShader->Render(commandList, *sceneObjects[0]);

	// Set the render target and viewport
	commandList.SetRenderTarget(m_DeferredRenderTarget);
	commandList.SetViewport(m_DeferredRenderTarget.GetViewport());
	commandList.SetScissorRect(m_ScissorRect);

	// Render skybox.
	m_Skybox.Render(commandList, camera);

	// Set first pass PSO.
	commandList.SetPipelineState(m_FirstPassPSO);
	commandList.SetGraphicsRootSignature(m_FirstPassRootSignature);
}

void Raven::DeferredRenderPath::Render(CommandList& commandList, LightProperties lightProps, std::vector<PointLight>& m_PointLights, std::vector<SpotLight>& m_SpotLights)
{
	commandList.SetPipelineState(m_ShadePSO);
	commandList.SetGraphicsRootSignature(m_ShadeRootSignature);
	auto game = Application::Get().GetGameRef();

	// Upload lights
	commandList.SetGraphics32BitConstants(RootParameters::LightPropertiesCB, lightProps);
	commandList.SetGraphicsDynamicStructuredBuffer(RootParameters::PointLights, m_PointLights);
	commandList.SetGraphicsDynamicStructuredBuffer(RootParameters::SpotLights, m_SpotLights);

	commandList.SetShaderResourceView(RootParameters::Textures, 0, m_DeferredRenderTarget.GetTexture(Raven::Color0), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
	if(!game->bump)
		commandList.SetShaderResourceView(RootParameters::Textures, 1, m_DeferredRenderTarget.GetTexture(Raven::Color1), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
	if (game->bump)
		commandList.SetShaderResourceView(RootParameters::Textures, 1, m_DeferredRenderTarget.GetTexture(Raven::Color4), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
	commandList.SetShaderResourceView(RootParameters::Textures, 2, m_DeferredRenderTarget.GetTexture(Raven::Color2), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
	commandList.SetShaderResourceView(RootParameters::Textures, 3, m_DeferredRenderTarget.GetTexture(Raven::Color3), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
	// Pass the light depth buffer to the shader.
	commandList.SetShaderResourceView(RootParameters::Textures, 4, m_DepthShader->GetDepthTexture(), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
	commandList.Draw(3);
}

void Raven::DeferredRenderPath::PostRender(CommandList& commandList)
{
	auto game = Application::Get().GetGameRef();
	// Perform HDR -> SDR tonemapping directly to the Window's render target.
	commandList.SetRenderTarget(game->GetWindow()->GetRenderTarget());
	commandList.SetViewport(game->GetWindow()->GetRenderTarget().GetViewport());
	commandList.SetPipelineState(m_PostProcessPSO);
	commandList.SetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	commandList.SetGraphicsRootSignature(m_PostProcessRootSignature);
	commandList.SetGraphics32BitConstants(0, game->g_TonemapParameters);
	commandList.SetShaderResourceView(1, 0, m_DeferredRenderTarget.GetTexture(game->g_finalTexture), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
	commandList.Draw(3);
}

void Raven::DeferredRenderPath::Compose(CommandList& commandList)
{
}
