/**
 * An UploadBuffer provides a convenient method to upload resources to the GPU.
 */
#pragma once

#include "Engine/Defines.h"

#include <wrl.h>
#include "Engine/d3d12.h"

#include <memory>
#include <deque>