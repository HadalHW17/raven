#pragma once

// Interface for a rendering pipeline.
// All of the rendering routine will be moved here,
// And the game class will be just responsible in calling these methods.

#include "Renderer/CommandList.h"
#include "Renderer/RenderTarget.h"
#include "Renderer/Light.h"
#include "Engine/SceneObject.h"
#include "Renderer/Camera.h"

namespace Raven
{
	class RenderPath
	{
	public:

		virtual void SetUpRenderTarget(CommandList& commandList) = 0;

		virtual void InitPSO(CommandList& commandList) = 0;

		virtual void ClearRenderTarget(CommandList& commandList) = 0;

		virtual void PreRender(CommandList& commandList, const Camera& camera, std::vector<SpotLight>& m_SpotLights, std::vector<SceneObject*>& sceneObjects) = 0;

		virtual void Render(CommandList& commandList, struct LightProperties lightProps, std::vector<PointLight>& m_PointLights, std::vector<SpotLight>& m_SpotLights) = 0;

		virtual void PostRender(CommandList& commandList) = 0;

		virtual void Compose(CommandList& commandList) = 0;

		virtual RenderTarget& GetRenderTarget() = 0;
	};
}
