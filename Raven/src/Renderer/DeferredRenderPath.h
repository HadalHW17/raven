#pragma once

// Deferred rendering pipeline.
#include "Renderer/RenderPath.h"
#include "Renderer/RootSignature.h"
#include "Renderer/RenderTarget.h"
#include "Renderer/Camera.h"
#include "Skybox.h"
#include "Renderer/Light.h"
#include "Engine/SceneObject.h"

#include "Raven/Raven.h"
#include "Renderer/ShaderClasses/DepthShader.h"

#include <d3d12.h>

// Rendering is done in multiple passes, where the first one just collects vertex data that will be needed
// And write it to different textures.
// Second pass that will perform shading based on the textures from the first pass.

namespace Raven
{

	struct LightProperties
	{
		uint32_t NumPointLights;
		uint32_t NumSpotLights;
	};

	// An enum for root signature parameters for the second render pass.
	// I'm not using scoped enums to avoid the explicit cast that would be required
	// to use these as root indices in the root signature.
	enum RootParameters
	{
		MatricesCB,         // ConstantBuffer<Mat> MatCB : register(b0);
		MaterialCB,         // ConstantBuffer<Material> MaterialCB : register( b0, space1 );
		LightPropertiesCB,  // ConstantBuffer<LightProperties> LightPropertiesCB : register( b1 );
		PointLights,        // StructuredBuffer<PointLight> PointLights : register( t0 );
		SpotLights,         // StructuredBuffer<SpotLight> SpotLights : register( t1 );
		Textures,           // Texture2D DiffuseTexture : register( t2 );
		NumRootParameters
	};

	class RAVEN_API DeferredRenderPath : public RenderPath
	{
	public:

		DeferredRenderPath() {}

		// Inherited via RenderPath
		virtual void SetUpRenderTarget(CommandList& commandList) override;
		virtual void InitPSO(CommandList& commandList) override;
		virtual void ClearRenderTarget(CommandList& commandList) override;
		virtual void PreRender(CommandList& commandList, const Camera& camera, std::vector<SpotLight>& m_SpotLights, std::vector<SceneObject*>& sceneObjects) override;
		virtual void Render(CommandList& commandList, struct LightProperties lightProps, std::vector<PointLight>& m_PointLights, std::vector<SpotLight>& m_SpotLights) override;
		virtual void PostRender(CommandList& commandList) override;
		virtual void Compose(CommandList& commandList) override;

		virtual RenderTarget& GetRenderTarget() override { return m_DeferredRenderTarget; };

		// We will also need scissors for rendering.
		D3D12_RECT m_ScissorRect;

	private:
		// One render target that will contain all of the textures that we need for deferred rendering.
		RenderTarget m_DeferredRenderTarget;

		std::shared_ptr<Raven::DepthShader> m_DepthShader;
		// For deferred rendering we are going to need 3 root signatures and 3 pipeline states
		// The first one will do the first render pass that will write normal, albedo, etc to textures
		Microsoft::WRL::ComPtr<ID3D12PipelineState> m_FirstPassPSO;
		RootSignature m_FirstPassRootSignature;

		// The second one will be responsible for shading in HDR.
		Microsoft::WRL::ComPtr<ID3D12PipelineState> m_ShadePSO;
		RootSignature m_ShadeRootSignature;

		// And the third one will perform tone mapping from HDR -> SDR, as well as some additional postprocessing.
		Microsoft::WRL::ComPtr<ID3D12PipelineState> m_PostProcessPSO;
		RootSignature m_PostProcessRootSignature;

		// We might also render skybox here.
		Skybox m_Skybox;


	};
}