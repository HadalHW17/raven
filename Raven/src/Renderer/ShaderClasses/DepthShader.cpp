#include "DepthShader.h"

#include "Engine/Helpers.h"
#include "Engine/d3dx12.h"
#include <d3dcompiler.h>
#include "Engine/Application.h"

#include "Engine/SceneObject.h"
#include "Renderer/Mesh.h"
#include "Engine/Game.h"


using namespace Microsoft::WRL;


enum RootParameters
{
	Matrix,
	Texture,
	NumRootParameters
};

Raven::DepthShader::DepthShader()
	:m_ScissorRect(CD3DX12_RECT(0, 0, LONG_MAX, LONG_MAX))
{
	// Get desired dimensions for our render target.
	int rtWidth = Application::Get().GetGameRef()->GetClientWidth();
	int rtHeigth = Application::Get().GetGameRef()->GetClientHeight();

	// Create an intermediate render target format.
	// For more precise depth data, make it 32-bits.
	DXGI_FORMAT HDRFormat = DXGI_FORMAT_R32G32B32A32_FLOAT;

	// Create an intermediate depth buffer format.
	DXGI_FORMAT depthBufferFormat = DXGI_FORMAT_D32_FLOAT;

	// Create an off-screen render target with a single color buffer and a depth buffer.
	auto colorDesc = CD3DX12_RESOURCE_DESC::Tex2D(HDRFormat, rtWidth, rtHeigth);
	colorDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;

	// Create description for depth buffer.
	auto depthDesc = CD3DX12_RESOURCE_DESC::Tex2D(depthBufferFormat, rtWidth, rtHeigth);
	depthDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	// Clear colour value for colour textures.
	D3D12_CLEAR_VALUE colorClearValue;
	colorClearValue.Format = colorDesc.Format;
	colorClearValue.Color[0] = 0.4f;
	colorClearValue.Color[1] = 0.6f;
	colorClearValue.Color[2] = 0.9f;
	colorClearValue.Color[3] = 1.0f;

	// Clear value for the depth buffer.
	D3D12_CLEAR_VALUE depthClearValue;
	depthClearValue.Format = depthDesc.Format;
	depthClearValue.DepthStencil = { 1.0f, 0 };

	// Texture for colour data of intersected scene.
	Raven::Texture DepthTexture = Raven::Texture(colorDesc, &colorClearValue,
		Raven::TextureUsage::RenderTarget,
		L"Depth Texture");

	// Create a depth buffer for the deferred rendering render target.
	Raven::Texture DepthBuffer = Raven::Texture(depthDesc, &depthClearValue,
		Raven::TextureUsage::Depth, L"Depth Buffer");

	m_DepthRenderTarget.AttachTexture(AttachmentPoint::Color0, DepthTexture);
	m_DepthRenderTarget.AttachTexture(AttachmentPoint::DepthStencil, DepthBuffer);
}

Raven::DepthShader::DepthShader(const DepthShader&)
{
}

Raven::DepthShader::~DepthShader()
{
}

void Raven::DepthShader::InitializeShader(CommandList&)
{
	auto device = Application::Get().GetDevice();

	

	D3D12_FEATURE_DATA_ROOT_SIGNATURE featureData = {};
	featureData.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_1;
	if (FAILED(device->CheckFeatureSupport(D3D12_FEATURE_ROOT_SIGNATURE, &featureData, sizeof(featureData))))
	{
		featureData.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_0;
	}

	// Vertex and pixel shaders for depth rendering.
	ComPtr<ID3DBlob> vs;
	ComPtr<ID3DBlob> ps;

	ThrowIfFailed(D3DReadFileToBlob(L"data/shaders/Sandbox/Depth_VS.cso", &vs));
	ThrowIfFailed(D3DReadFileToBlob(L"data/shaders/Sandbox/Depth_PS.cso", &ps));

	// Allow input layout and deny unnecessary access to certain pipeline stages.
	D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS;

	CD3DX12_DESCRIPTOR_RANGE1 descriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 2);

	CD3DX12_ROOT_PARAMETER1 rootParameters[NumRootParameters];
	// We only need matrices for a to generate depth texture.
	rootParameters[Matrix].InitAsConstantBufferView(0, 0, D3D12_ROOT_DESCRIPTOR_FLAG_NONE, D3D12_SHADER_VISIBILITY_VERTEX);
	rootParameters[RootParameters::Texture].InitAsDescriptorTable(1, &descriptorRange, D3D12_SHADER_VISIBILITY_PIXEL);


	// Initialize root signature without any texture samplers.
	CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDescription;
	rootSignatureDescription.Init_1_1(NumRootParameters, rootParameters, 0, nullptr, rootSignatureFlags);

	m_DepthRootSignature.SetRootSignatureDesc(rootSignatureDescription.Desc_1_1, featureData.HighestVersion);

	// Setup the first pass pipeline state.
	struct DepthPipelineStateStream
	{
		CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE pRootSignature;
		CD3DX12_PIPELINE_STATE_STREAM_INPUT_LAYOUT InputLayout;
		CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
		CD3DX12_PIPELINE_STATE_STREAM_VS VS;
		CD3DX12_PIPELINE_STATE_STREAM_PS PS;
		CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL_FORMAT DSVFormat;
		CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
	} depthPipelineStateStream;

	depthPipelineStateStream.pRootSignature = m_DepthRootSignature.GetRootSignature().Get();
	depthPipelineStateStream.InputLayout = { VertexPositionNormalTexture::InputElements, VertexPositionNormalTexture::InputElementCount };
	depthPipelineStateStream.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	depthPipelineStateStream.VS = CD3DX12_SHADER_BYTECODE(vs.Get());
	depthPipelineStateStream.PS = CD3DX12_SHADER_BYTECODE(ps.Get());
	depthPipelineStateStream.DSVFormat = m_DepthRenderTarget.GetDepthStencilFormat();
	depthPipelineStateStream.RTVFormats = m_DepthRenderTarget.GetRenderTargetFormats();

	D3D12_PIPELINE_STATE_STREAM_DESC depthPipelineStateStreamDesc = {
		sizeof(DepthPipelineStateStream), &depthPipelineStateStream
	};

	// Create the pipeline state
	ThrowIfFailed(device->CreatePipelineState(&depthPipelineStateStreamDesc, IID_PPV_ARGS(&m_DepthPipelineState)));
}

bool Raven::DepthShader::SetShaderParameters(CommandList& commandList, DirectX::XMMATRIX model, DirectX::XMMATRIX view, DirectX::XMMATRIX projection)
{
	// Set depth PSO.
	commandList.SetScissorRect(m_ScissorRect);
	commandList.SetPipelineState(m_DepthPipelineState);
	commandList.SetGraphicsRootSignature(m_DepthRootSignature);

	// Compute MVP matrix on the CPU, since for depth data, we don't need anything else.
	DirectX::XMMATRIX modelViewProjectionMatrix = model * view * projection;
	commandList.SetGraphicsDynamicConstantBuffer(Matrix, modelViewProjectionMatrix);

	return true;
}

void Raven::DepthShader::ClearRenderTarget(CommandList& commandList)
{
	FLOAT clearColor[] = { 0.4f, 0.6f, 0.9f, 1.0f };

	commandList.ClearTexture(m_DepthRenderTarget.GetTexture(Raven::AttachmentPoint::Color0), clearColor);
	commandList.ClearDepthStencilTexture(m_DepthRenderTarget.GetTexture(Raven::AttachmentPoint::DepthStencil), D3D12_CLEAR_FLAG_DEPTH);
}

void Raven::DepthShader::Render(CommandList& commandList, SceneObject& sceneObject)
{
	commandList.SetRenderTarget(m_DepthRenderTarget);
	commandList.SetViewport(m_DepthRenderTarget.GetViewport());
	sceneObject.Render(commandList);
}
