#pragma once

#include "Renderer/RootSignature.h"
#include "Renderer/RenderTarget.h"

#include <d3d12.h>
#include <DirectXMath.h>


namespace Raven
{
	class DepthShader
	{
	public:

		DepthShader();
		DepthShader(const DepthShader&);
		~DepthShader();

		void InitializeShader(class CommandList&);
		bool SetShaderParameters(class CommandList&, DirectX::XMMATRIX, DirectX::XMMATRIX, DirectX::XMMATRIX);

		void ClearRenderTarget(class CommandList&);
		
		void Render(class CommandList&, class SceneObject&);

		inline RootSignature GetRootSignature() { return m_DepthRootSignature; }
		inline Microsoft::WRL::ComPtr <ID3D12PipelineState> GetPSO() { return m_DepthPipelineState; }

		inline RenderTarget GetRenderTarget() { return m_DepthRenderTarget; }
		inline Texture GetDepthTexture() { return m_DepthRenderTarget.GetTexture(Color0); }

	private:
		// Pipeline state for depth shader.
		Microsoft::WRL::ComPtr<ID3D12PipelineState> m_DepthPipelineState;

		// Root signature for depth shader.
		RootSignature m_DepthRootSignature;

		// Output depth texture.
		RenderTarget m_DepthRenderTarget;

		// Scissors.
		D3D12_RECT m_ScissorRect;
	};
}