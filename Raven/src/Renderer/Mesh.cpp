#include "rvpch.h"
#include "Mesh.h"

#include "Engine/Application.h"
/* assimp include files. These three are usually needed. */
#include <assimp/mesh.h>
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>


using namespace DirectX;
using namespace Microsoft::WRL;

namespace Raven
{
	const D3D12_INPUT_ELEMENT_DESC VertexPositionNormalTexture::InputElements[] =
	{
		{ "POSITION",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "NORMAL",     0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD",   0, DXGI_FORMAT_R32G32_FLOAT,    0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "BINORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT,    0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "TANGENT",	0, DXGI_FORMAT_R32G32B32_FLOAT,    0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};

	Mesh::Mesh()
		: m_IndexCount(0)
	{}

	Mesh::~Mesh()
	{
		// Allocated resources will be cleaned automatically when the pointers go out of scope.
	}

	void Mesh::Render(CommandList& commandList, uint32_t instanceCount, uint32_t firstInstance)
	{
		commandList.SetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		commandList.SetVertexBuffer(0, m_VertexBuffer);

		if (m_IndexCount > 0)
		{
			commandList.SetIndexBuffer(m_IndexBuffer);
			commandList.DrawIndexed(m_IndexCount, instanceCount, 0, 0, firstInstance);
		}
		else
		{
			commandList.Draw(m_VertexBuffer.GetNumVertices(), instanceCount, 0, firstInstance);
		}
	}

	std::unique_ptr<Mesh> Mesh::CreateSphere(CommandList& commandList, float diameter, size_t tessellation, bool rhcoords)
	{
		VertexCollection vertices;
		IndexCollection indices;

		if (tessellation < 3)
			throw std::out_of_range("tessellation parameter out of range");

		float radius = diameter / 2.0f;
		size_t verticalSegments = tessellation;
		size_t horizontalSegments = tessellation * 2;

		// Create rings of vertices at progressively higher latitudes.
		for (size_t i = 0; i <= verticalSegments; i++)
		{
			float v = 1 - (float)i / verticalSegments;

			float latitude = (i * XM_PI / verticalSegments) - XM_PIDIV2;
			float dy, dxz;

			XMScalarSinCos(&dy, &dxz, latitude);

			// Create a single ring of vertices at this latitude.
			for (size_t j = 0; j <= horizontalSegments; j++)
			{
				float u = (float)j / horizontalSegments;

				float longitude = j * XM_2PI / horizontalSegments;
				float dx, dz;

				XMScalarSinCos(&dx, &dz, longitude);

				dx *= dxz;
				dz *= dxz;

				XMVECTOR normal = XMVectorSet(dx, dy, dz, 0);
				XMVECTOR textureCoordinate = XMVectorSet(u, v, 0, 0);

				vertices.push_back(VertexPositionNormalTexture(normal * radius, normal, textureCoordinate));
			}
		}

		// Fill the index buffer with triangles joining each pair of latitude rings.
		size_t stride = horizontalSegments + 1;

		for (size_t i = 0; i < verticalSegments; i++)
		{
			for (size_t j = 0; j <= horizontalSegments; j++)
			{
				size_t nextI = i + 1;
				size_t nextJ = (j + 1) % stride;

				indices.push_back(static_cast<uint16_t>(i * stride + j));
				indices.push_back(static_cast<uint16_t>(nextI * stride + j));
				indices.push_back(static_cast<uint16_t>(i * stride + nextJ));

				indices.push_back(static_cast<uint16_t>(i * stride + nextJ));
				indices.push_back(static_cast<uint16_t>(nextI * stride + j));
				indices.push_back(static_cast<uint16_t>(nextI * stride + nextJ));
			}
		}

		// Create the primitive object.
		std::unique_ptr<Mesh> mesh(new Mesh());

		mesh->Initialize(commandList, vertices, indices, rhcoords);

		return mesh;
	}

	std::unique_ptr<Mesh> Mesh::CreateCube(CommandList& commandList, float size, bool rhcoords)
	{
		// A cube has six faces, each one pointing in a different direction.
		const int FaceCount = 6;

		static const XMVECTORF32 faceNormals[FaceCount] =
		{
			{ 0,  0,  1 },
			{ 0,  0, -1 },
			{ 1,  0,  0 },
			{ -1,  0,  0 },
			{ 0,  1,  0 },
			{ 0, -1,  0 },
		};

		static const XMVECTORF32 textureCoordinates[4] =
		{
			{ 1, 0 },
			{ 1, 1 },
			{ 0, 1 },
			{ 0, 0 },
		};

		VertexCollection vertices;
		IndexCollection indices;

		size /= 2;

		// Create each face in turn.
		for (int i = 0; i < FaceCount; i++)
		{
			XMVECTOR normal = faceNormals[i];

			// Get two vectors perpendicular both to the face normal and to each other.
			XMVECTOR basis = (i >= 4) ? g_XMIdentityR2 : g_XMIdentityR1;

			XMVECTOR side1 = XMVector3Cross(normal, basis);
			XMVECTOR side2 = XMVector3Cross(normal, side1);

			// Six indices (two triangles) per face.
			size_t vbase = vertices.size();
			indices.push_back(static_cast<uint16_t>(vbase + 0));
			indices.push_back(static_cast<uint16_t>(vbase + 1));
			indices.push_back(static_cast<uint16_t>(vbase + 2));

			indices.push_back(static_cast<uint16_t>(vbase + 0));
			indices.push_back(static_cast<uint16_t>(vbase + 2));
			indices.push_back(static_cast<uint16_t>(vbase + 3));

			// Four vertices per face.
			vertices.push_back(VertexPositionNormalTexture((normal - side1 - side2) * size, normal, textureCoordinates[0]));
			vertices.push_back(VertexPositionNormalTexture((normal - side1 + side2) * size, normal, textureCoordinates[1]));
			vertices.push_back(VertexPositionNormalTexture((normal + side1 + side2) * size, normal, textureCoordinates[2]));
			vertices.push_back(VertexPositionNormalTexture((normal + side1 - side2) * size, normal, textureCoordinates[3]));
		}

		// Create the primitive object.
		std::unique_ptr<Mesh> mesh(new Mesh());

		mesh->Initialize(commandList, vertices, indices, rhcoords);

		return mesh;
	}

	// Helper computes a point on a unit circle, aligned to the x/z plane and centered on the origin.
	static inline XMVECTOR GetCircleVector(size_t i, size_t tessellation)
	{
		float angle = i * XM_2PI / tessellation;
		float dx, dz;

		XMScalarSinCos(&dx, &dz, angle);

		XMVECTORF32 v = { dx, 0, dz, 0 };
		return v;
	}

	static inline XMVECTOR GetCircleTangent(size_t i, size_t tessellation)
	{
		float angle = (i * XM_2PI / tessellation) + XM_PIDIV2;
		float dx, dz;

		XMScalarSinCos(&dx, &dz, angle);

		XMVECTORF32 v = { dx, 0, dz, 0 };
		return v;
	}

	// Helper creates a triangle fan to close the end of a cylinder / cone
	static void CreateCylinderCap(VertexCollection& vertices, IndexCollection& indices, size_t tessellation, float height, float radius, bool isTop)
	{
		// Create cap indices.
		for (size_t i = 0; i < tessellation - 2; i++)
		{
			size_t i1 = (i + 1) % tessellation;
			size_t i2 = (i + 2) % tessellation;

			if (isTop)
			{
				std::swap(i1, i2);
			}

			size_t vbase = vertices.size();
			indices.push_back(static_cast<uint16_t>(vbase));
			indices.push_back(static_cast<uint16_t>(vbase + i1));
			indices.push_back(static_cast<uint16_t>(vbase + i2));
		}

		// Which end of the cylinder is this?
		XMVECTOR normal = g_XMIdentityR1;
		XMVECTOR textureScale = g_XMNegativeOneHalf;

		if (!isTop)
		{
			normal = -normal;
			textureScale *= g_XMNegateX;
		}

		// Create cap vertices.
		for (size_t i = 0; i < tessellation; i++)
		{
			XMVECTOR circleVector = GetCircleVector(i, tessellation);

			XMVECTOR position = (circleVector * radius) + (normal * height);

			XMVECTOR textureCoordinate = XMVectorMultiplyAdd(XMVectorSwizzle<0, 2, 3, 3>(circleVector), textureScale, g_XMOneHalf);

			vertices.push_back(VertexPositionNormalTexture(position, normal, textureCoordinate));
		}
	}

	std::unique_ptr<Mesh> Mesh::CreateCone(CommandList& commandList, float diameter, float height, size_t tessellation, bool rhcoords)
	{
		VertexCollection vertices;
		IndexCollection indices;

		if (tessellation < 3)
			throw std::out_of_range("tessellation parameter out of range");

		height /= 2;

		XMVECTOR topOffset = g_XMIdentityR1 * height;

		float radius = diameter / 2;
		size_t stride = tessellation + 1;

		// Create a ring of triangles around the outside of the cone.
		for (size_t i = 0; i <= tessellation; i++)
		{
			XMVECTOR circlevec = GetCircleVector(i, tessellation);

			XMVECTOR sideOffset = circlevec * radius;

			float u = (float)i / tessellation;

			XMVECTOR textureCoordinate = XMLoadFloat(&u);

			XMVECTOR pt = sideOffset - topOffset;

			XMVECTOR normal = XMVector3Cross(GetCircleTangent(i, tessellation), topOffset - pt);
			normal = XMVector3Normalize(normal);

			// Duplicate the top vertex for distinct normals
			vertices.push_back(VertexPositionNormalTexture(topOffset, normal, g_XMZero));
			vertices.push_back(VertexPositionNormalTexture(pt, normal, textureCoordinate + g_XMIdentityR1));

			indices.push_back(static_cast<uint16_t>(i * 2));
			indices.push_back(static_cast<uint16_t>((i * 2 + 3) % (stride * 2)));
			indices.push_back(static_cast<uint16_t>((i * 2 + 1) % (stride * 2)));
		}

		// Create flat triangle fan caps to seal the bottom.
		CreateCylinderCap(vertices, indices, tessellation, height, radius, false);

		// Create the primitive object.
		std::unique_ptr<Mesh> mesh(new Mesh());

		mesh->Initialize(commandList, vertices, indices, rhcoords);

		return mesh;
	}

	std::unique_ptr<Mesh> Mesh::CreateTorus(CommandList& commandList, float diameter, float thickness, size_t tessellation, bool rhcoords)
	{
		VertexCollection vertices;
		IndexCollection indices;

		if (tessellation < 3)
			throw std::out_of_range("tessellation parameter out of range");

		size_t stride = tessellation + 1;

		// First we loop around the main ring of the torus.
		for (size_t i = 0; i <= tessellation; i++)
		{
			float u = (float)i / tessellation;

			float outerAngle = i * XM_2PI / tessellation - XM_PIDIV2;

			// Create a transform matrix that will align geometry to
			// slice perpendicularly though the current ring position.
			XMMATRIX transform = XMMatrixTranslation(diameter / 2, 0, 0) * XMMatrixRotationY(outerAngle);

			// Now we loop along the other axis, around the side of the tube.
			for (size_t j = 0; j <= tessellation; j++)
			{
				float v = 1 - (float)j / tessellation;

				float innerAngle = j * XM_2PI / tessellation + XM_PI;
				float dx, dy;

				XMScalarSinCos(&dy, &dx, innerAngle);

				// Create a vertex.
				XMVECTOR normal = XMVectorSet(dx, dy, 0, 0);
				XMVECTOR position = normal * thickness / 2;
				XMVECTOR textureCoordinate = XMVectorSet(u, v, 0, 0);

				position = XMVector3Transform(position, transform);
				normal = XMVector3TransformNormal(normal, transform);

				vertices.push_back(VertexPositionNormalTexture(position, normal, textureCoordinate));

				// And create indices for two triangles.
				size_t nextI = (i + 1) % stride;
				size_t nextJ = (j + 1) % stride;

				indices.push_back(static_cast<uint16_t>(i * stride + j));
				indices.push_back(static_cast<uint16_t>(i * stride + nextJ));
				indices.push_back(static_cast<uint16_t>(nextI * stride + j));

				indices.push_back(static_cast<uint16_t>(i * stride + nextJ));
				indices.push_back(static_cast<uint16_t>(nextI * stride + nextJ));
				indices.push_back(static_cast<uint16_t>(nextI * stride + j));
			}
		}

		// Create the primitive object.
		std::unique_ptr<Mesh> mesh(new Mesh());

		mesh->Initialize(commandList, vertices, indices, rhcoords);

		return mesh;
	}

	std::unique_ptr<Mesh> Mesh::CreatePlane(CommandList& commandList, float width, float height, bool rhcoords)
	{
		VertexCollection vertices =
		{
			{ XMFLOAT3(-0.5f * width, 0.0f,  0.5f * height), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f) }, // 0
			{ XMFLOAT3(0.5f * width, 0.0f,  0.5f * height), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 0.0f) }, // 1
			{ XMFLOAT3(0.5f * width, 0.0f, -0.5f * height), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 1.0f) }, // 2
			{ XMFLOAT3(-0.5f * width, 0.0f, -0.5f * height), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f) }  // 3
		};

		IndexCollection indices =
		{
			0, 3, 1, 1, 3, 2
		};

		std::unique_ptr<Mesh> mesh(new Mesh());

		mesh->Initialize(commandList, vertices, indices, rhcoords);

		return mesh;
	}

	std::unique_ptr<Mesh> Mesh::LoadFromFIle(CommandList& commandList, const char* fileName, bool rhCoords)
	{
		VertexCollection vertecies;
		IndexCollection faces;

		// Load mesh from file.
		auto scene = aiImportFile(fileName, aiProcessPreset_TargetRealtime_MaxQuality);
		
		for (int m_num = 0; m_num < scene->mNumMeshes; ++m_num)
		{
			auto mesh = scene->mMeshes[m_num];

			// Load vertex data.
			for (int i = 0; i < mesh->mNumVertices; ++i)
			{
				VertexPositionNormalTexture vertexData;
				vertexData.position.x = mesh->mVertices[i].x;
				vertexData.position.y = mesh->mVertices[i].y;
				vertexData.position.z = mesh->mVertices[i].z;
				vertexData.normal.x = mesh->mNormals[i].x;
				vertexData.normal.y = mesh->mNormals[i].y;
				vertexData.normal.z = mesh->mNormals[i].z;
				if (mesh->HasTextureCoords(0))
				{
					vertexData.textureCoordinate.x = mesh->mTextureCoords[0][i].x;
					vertexData.textureCoordinate.y = mesh->mTextureCoords[0][i].y;
				}
				vertecies.push_back(vertexData);
			}

			// Load index data.
			for (int i = 0; i < mesh->mNumFaces; ++i)
			{
				faces.push_back(mesh->mFaces[i].mIndices[0]);
				faces.push_back(mesh->mFaces[i].mIndices[1]);
				faces.push_back(mesh->mFaces[i].mIndices[2]);
			}
		}


		std::unique_ptr<Mesh> rvMesh(new Mesh());

		rvMesh->Initialize(commandList, vertecies, faces, rhCoords);
		return rvMesh;
	}

	void CalculateTangentBinormal(VertexPositionNormalTexture &vertex1, VertexPositionNormalTexture &vertex2, VertexPositionNormalTexture &vertex3)
	{
		DirectX::XMFLOAT3 tangent, binormal;
		float vector1[3], vector2[3];
		float tuVector[2], tvVector[2];
		float den;
		float length;


		// Calculate the two vectors for this face.
		vector1[0] = vertex2.position.x - vertex1.position.x;
		vector1[1] = vertex2.position.y - vertex1.position.y;
		vector1[2] = vertex2.position.z - vertex1.position.z;

		vector2[0] = vertex3.position.x - vertex1.position.x;
		vector2[1] = vertex3.position.y - vertex1.position.y;
		vector2[2] = vertex3.position.z - vertex1.position.z;

		// Calculate the tu and tv texture space vectors.
		tuVector[0] = vertex2.textureCoordinate.x - vertex1.textureCoordinate.x;
		tvVector[0] = vertex2.textureCoordinate.y - vertex1.textureCoordinate.y;

		tuVector[1] = vertex3.textureCoordinate.x - vertex1.textureCoordinate.x;
		tvVector[1] = vertex3.textureCoordinate.y - vertex1.textureCoordinate.y;

		// Calculate the denominator of the tangent/binormal equation.
		den = 1.0f / (tuVector[0] * tvVector[1] - tuVector[1] * tvVector[0]);

		// Calculate the cross products and multiply by the coefficient to get the tangent and binormal.
		tangent.x = (tvVector[1] * vector1[0] - tvVector[0] * vector2[0]) * den;
		tangent.y = (tvVector[1] * vector1[1] - tvVector[0] * vector2[1]) * den;
		tangent.z = (tvVector[1] * vector1[2] - tvVector[0] * vector2[2]) * den;

		binormal.x = (tuVector[0] * vector2[0] - tuVector[1] * vector1[0]) * den;
		binormal.y = (tuVector[0] * vector2[1] - tuVector[1] * vector1[1]) * den;
		binormal.z = (tuVector[0] * vector2[2] - tuVector[1] * vector1[2]) * den;

		// Calculate the length of this normal.
		length = sqrt((tangent.x * tangent.x) + (tangent.y * tangent.y) + (tangent.z * tangent.z));

		// Normalize the normal and then store it
		tangent.x = tangent.x / length;
		tangent.y = tangent.y / length;
		tangent.z = tangent.z / length;

		// Calculate the length of this normal.
		length = sqrt((binormal.x * binormal.x) + (binormal.y * binormal.y) + (binormal.z * binormal.z));

		// Normalize the normal and then store it
		binormal.x = binormal.x / length;
		binormal.y = binormal.y / length;
		binormal.z = binormal.z / length;

		vertex1.tangent = tangent;
		vertex2.tangent = tangent;
		vertex3.tangent = tangent;

		vertex1.binormal = binormal;
		vertex2.binormal = binormal;
		vertex3.binormal = binormal;

		return;
	}

	std::unique_ptr<Mesh> Mesh::ConvertFromAssimp(CommandList& commandList, const aiMesh* mesh, bool rhCoords, const Texture &texture, const Texture& normalMap)
	{
		VertexCollection vertecies;
		IndexCollection faces;

		// Load mesh from assimp structure.
		// Load vertex data.
		for (int i = 0; i < mesh->mNumVertices; ++i)
		{
			VertexPositionNormalTexture vertexData;
			vertexData.position.x = mesh->mVertices[i].x;
			vertexData.position.y = mesh->mVertices[i].y;
			vertexData.position.z = mesh->mVertices[i].z;
			vertexData.normal.x = mesh->mNormals[i].x;
			vertexData.normal.y = mesh->mNormals[i].y;
			vertexData.normal.z = mesh->mNormals[i].z;

			vertexData.tangent.x = mesh->mTangents[i].x;
			vertexData.tangent.y = mesh->mTangents[i].y;
			vertexData.tangent.z = mesh->mTangents[i].z;
			vertexData.binormal.x = mesh->mBitangents[i].x;
			vertexData.binormal.y = mesh->mBitangents[i].y;
			vertexData.binormal.z = mesh->mBitangents[i].z;
			if (mesh->HasTextureCoords(0))
			{
				vertexData.textureCoordinate.x = mesh->mTextureCoords[0][i].x;
				vertexData.textureCoordinate.y = 1 - mesh->mTextureCoords[0][i].y;
			}
			vertecies.push_back(vertexData);
			
		}

		// Load index data.
		for (int i = 0; i < mesh->mNumFaces; ++i)
		{
			faces.push_back(mesh->mFaces[i].mIndices[0]);
			faces.push_back(mesh->mFaces[i].mIndices[1]);
			faces.push_back(mesh->mFaces[i].mIndices[2]);

			CalculateTangentBinormal(vertecies[mesh->mFaces[i].mIndices[0]], vertecies[mesh->mFaces[i].mIndices[1]], vertecies[mesh->mFaces[i].mIndices[2]]);
		}

		std::unique_ptr<Mesh> rvMesh(new Mesh());

		rvMesh->Initialize(commandList, vertecies, faces, rhCoords);
		rvMesh->m_Texture = texture;
		rvMesh->m_NormalMap = normalMap;
		return rvMesh;
	}


	// Helper for flipping winding of geometric primitives for LH vs. RH coords
	static void ReverseWinding(IndexCollection& indices, VertexCollection& vertices)
	{
		assert((indices.size() % 3) == 0);
		for (auto it = indices.begin(); it != indices.end(); it += 3)
		{
			std::swap(*it, *(it + 2));
		}

		for (auto it = vertices.begin(); it != vertices.end(); ++it)
		{
			it->textureCoordinate.x = (1.f - it->textureCoordinate.x);
		}
	}

	void Mesh::Initialize(CommandList& commandList, VertexCollection& vertices, IndexCollection& indices, bool rhcoords)
	{
		/*if (vertices.size() >= USHRT_MAX)
			throw std::exception("Too many vertices for 16-bit index buffer");*/

		if (!rhcoords)
			ReverseWinding(indices, vertices);

		commandList.CopyVertexBuffer(m_VertexBuffer, vertices);
		commandList.CopyIndexBuffer(m_IndexBuffer, indices);

		m_IndexCount = static_cast<UINT>(indices.size());
	}
}