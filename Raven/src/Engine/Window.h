/**
* @brief A window for our application.
*/
#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <wrl.h>
#include <d3d12.h>
#include <dxgi1_5.h>

#include "HighResolutionClock.h"
#include "Raven/Raven.h"
#include "Renderer/RenderTarget.h"
#include "Renderer/GUI.h"



namespace Raven
{
	// Forward-declare the DirectXTemplate class.
	class Game;

	class RAVEN_API Window : public std::enable_shared_from_this<Window>
	{
	public:
		// Number of swapchain back buffers.
		static const UINT BufferCount = 3;

		/**
		* Get a handle to this window's instance.
		* @returns The handle to the window instance or nullptr if this is not a valid window.
		*/
		HWND GetWindowHandle() const;

		/**
		* Initialize the window.
		*/
		void Initialize();

		/**
		* Destroy this window.
		*/
		void Destroy();

		const std::wstring& GetWindowName() const;

		int GetClientWidth() const;
		int GetClientHeight() const;

		/**
		* Should this window be rendered with vertical refresh synchronization.
		*/
		bool IsVSync() const;
		void SetVSync(bool vSync);
		void ToggleVSync();

		/**
		* Is this a windowed window or full-screen?
		*/
		bool IsFullScreen() const;

		// Set the fullscreen state of the window.
		void SetFullscreen(bool fullscreen);
		void ToggleFullscreen();

		/**
		 * Show this window.
		 */
		void Show();

		/**
		 * Hide the window.
		 */
		void Hide();

		/**
		 * Return the current back buffer index.
		 */
		UINT GetCurrentBackBufferIndex() const;

		

		const RenderTarget& GetRenderTarget() const;


		/**
		 * Present the swapchain's back buffer to the screen.
		 * Returns the current back buffer index after the present.
		 */
		UINT Present(const Texture& texture = Texture());

		/**
		 * Get the render target view for the current back buffer.
		 */
		D3D12_CPU_DESCRIPTOR_HANDLE GetCurrentRenderTargetView() const;

		/**
		 * Get the back buffer resource for the current back buffer.
		 */
		Microsoft::WRL::ComPtr<ID3D12Resource> GetCurrentBackBuffer() const;

		Texture& GetCurrentBackBufferTexture();

		// Event callback.
		void set_event_callback(std::function<void(class Event&)> e) { function_callback = e; }
		void on_event(Event& e);
	protected:
		// The Window procedure needs to call protected methods of this class.
		friend LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

		// Only the application can create a window.
		friend class Application;
		// The DirectXTemplate class needs to register itself with a window.
		friend class Game;

		Window() = delete;
		Window(HWND hWnd, const std::wstring& windowName, int clientWidth, int clientHeight, bool vSync);
		virtual ~Window();

		// Register a Game with this window. This allows
		// the window to callback functions in the Game class.
		void RegisterCallbacks(std::shared_ptr<Game> pGame);

		// Update and Draw can only be called by the application.
		virtual bool OnUpdate(AppUpdateEvent& e);
		virtual bool OnRender(AppRenderEvent& e);

		// A keyboard key was pressed
		virtual bool OnKeyPressed(KeyPressedEvent& e);
		// A keyboard key was released
		virtual bool OnKeyReleased(KeyReleaseEvent& e);

		// The mouse was moved
		virtual bool OnMouseMoved(MouseMovedEvent& e);
		// A button on the mouse was pressed
		virtual bool OnMouseButtonPressed(MouseButtonPresedEvent& e);
		// A button on the mouse was released
		virtual bool OnMouseButtonReleased(MouseButtonReleasedEvent& e);
		// The mouse wheel was moved.
		virtual bool OnMouseWheel(MouseScrolledEvent& e);

		// The window was resized.
		virtual bool OnResize(WindowResizedEvent& e);

		// Create the swapchian.
		Microsoft::WRL::ComPtr<IDXGISwapChain4> CreateSwapChain();

		// Update the render target views for the swapchain back buffers.
		void UpdateRenderTargetViews();

	private:
		// Windows should not be copied.
		Window(const Window& copy) = delete;
		Window& operator=(const Window& other) = delete;

		HWND m_hWnd;

		std::wstring m_WindowName;

		int m_ClientWidth;
		int m_ClientHeight;
		bool m_VSync;
		bool m_Fullscreen;

		UINT64 m_FenceValues[BufferCount];
		uint64_t m_FrameValues[BufferCount];

		HighResolutionClock m_UpdateClock;
		HighResolutionClock m_RenderClock;

		std::weak_ptr<Game> m_pGame;

		Microsoft::WRL::ComPtr<IDXGISwapChain4> m_dxgiSwapChain;
		Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> m_d3d12RTVDescriptorHeap;
		Microsoft::WRL::ComPtr<ID3D12Resource> m_d3d12BackBuffers[BufferCount];

		UINT m_RTVDescriptorSize;
		UINT m_CurrentBackBufferIndex;

		RECT m_WindowRect;
		bool m_IsTearingSupported;

		int m_PreviousMouseX;
		int m_PreviousMouseY;

		// Event listener.
		std::function<void(class Event&)> function_callback; 
		Texture m_BackBufferTextures[BufferCount];
		// Marked mutable to allow modification in a const function.
		mutable RenderTarget m_RenderTarget;

		GUI m_GUI;
	};
}