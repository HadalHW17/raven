/**
 *   @brief The Game class is the abstract base class for DirecX 12 demos.
 */
#pragma once

#include <memory>
#include <string>
#include "Raven/Raven.h"
#include "Events/KeyEvent.h"
#include "Events/MouseEvent.h"
#include "Events/ApplicationEvent.h"
#include "Renderer/RenderTarget.h"
#include "Layers/LayerStack.h"
#include "Renderer/Tonemapping.h"


namespace Raven
{

	class Window;

	class RAVEN_API Game : public std::enable_shared_from_this<Game>
	{
	public:
		/**
		 * Create the DirectX demo using the specified window dimensions.
		 */
		Game(const std::wstring& name, int width, int height, bool vSync);
		virtual ~Game();

		int GetClientWidth() const
		{
			return m_Width;
		}

		int GetClientHeight() const
		{
			return m_Height;
		}

		/**
		 *  Initialize the DirectX Runtime.
		 */
		virtual bool Initialize();

		/**
		 *  Load content required for the demo.
		 */
		virtual bool LoadContent() = 0;

		/**
		 *  Unload demo specific content that was loaded in LoadContent.
		 */
		virtual void UnloadContent() = 0;

		/**
		 * Destroy any resource that are used by the game.
		 */
		virtual void Destroy();
		//------------------------Layer functions--------------------------
		/**
		* Add layer to the layer stack.
		*/
		void PushLayer(RLayer* layer);

		/**
		* Add UI layer to the end of the layer stack.
		*/
		void PushOverlay(RLayer* overlay);

		/**
		* Return layer stack.
		*/
		inline RLayerStack& GetLayerStack() { return m_layerStack; }
		//-----------------------------------------------------------------

		inline std::shared_ptr<Window> GetWindow() { return m_pWindow; }

		TonemapParameters g_TonemapParameters;

		double g_FPS;

		Raven::AttachmentPoint g_finalTexture = Color0;
		bool bump = false;

	protected:
		friend class Window;

		/**
		*  Invoked every time an event is dispatched.
		*/
		virtual bool OnEvent(Event& e);

		/**
		 *  Update the game logic.
		 */
		virtual bool OnUpdate(AppUpdateEvent& e);

		/**
		 *  Render stuff.
		 */
		virtual bool OnRender(AppRenderEvent& e);

		/**
		 * Invoked by the registered window when a key is pressed
		 * while the window has focus.
		 */
		virtual bool OnKeyPressed(KeyPressedEvent& e);

		/**
		 * Invoked when a key on the keyboard is released.
		 */
		virtual bool OnKeyReleased(KeyReleaseEvent& e);

		/**
		 * Invoked when the mouse is moved over the registered window.
		 */
		virtual bool OnMouseMoved(MouseMovedEvent& e);

		/**
		 * Invoked when a mouse button is pressed over the registered window.
		 */
		virtual bool OnMouseButtonPressed(MouseButtonPresedEvent& e);

		/**
		 * Invoked when a mouse button is released over the registered window.
		 */
		virtual bool OnMouseButtonReleased(MouseButtonReleasedEvent& e);

		/**
		 * Invoked when the mouse wheel is scrolled while the registered window has focus.
		 */
		virtual bool OnMouseWheel(MouseScrolledEvent& e);

		/**
		 * Invoked when the attached window is resized.
		 */
		virtual bool OnResize(WindowResizedEvent& e);

		/**
		 * Invoked when the registered window instance is destroyed.
		 */
		virtual void OnWindowDestroy();

		std::shared_ptr<Window> m_pWindow;
		int m_Width;
		int m_Height;
	private:
		std::wstring m_Name;

		bool m_vSync;

		// Stack that contains all of the game layers (from scene to UI).
		RLayerStack m_layerStack;

	};
}