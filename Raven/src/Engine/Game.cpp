#include <rvpch.h>

#include "Application.h"
#include "Game.h"
#include "Window.h"

namespace Raven
{

	Game::Game(const std::wstring& name, int width, int height, bool vSync)
		: m_Name(name)
		, m_Width(width)
		, m_Height(height)
		, m_vSync(vSync)
	{
	}

	Game::~Game()
	{
		assert(!m_pWindow && "Use Game::Destroy() before destruction.");
	}

	bool Game::Initialize()
	{
		// Check for DirectX Math library support.

		if (!DirectX::XMVerifyCPUSupport())
		{
			MessageBoxA(NULL, "Failed to verify DirectX Math library support.", "Error", MB_OK | MB_ICONERROR);
			return false;
		}

		m_pWindow = Application::Get().CreateRenderWindow(m_Name, m_Width, m_Height, m_vSync);
		m_pWindow->RegisterCallbacks(shared_from_this());
		m_pWindow->Show();

		return true;
	}

	void Game::Destroy()
	{
		Application::Get().DestroyWindow(m_pWindow);
		m_pWindow.reset();
	}

	void Game::PushLayer(RLayer* layer)
	{
		m_layerStack.push_layer(layer);
		layer->OnAttach();

	}

	void Game::PushOverlay(RLayer* overlay)
	{
		m_layerStack.push_overlay(overlay);
		overlay->OnAttach();

	}

	bool Game::OnEvent(Event& e)
	{
		EventDispatcher dispatcher(e);
		dispatcher.dispatch<AppUpdateEvent>(BIND_EVENT_FN(Game::OnUpdate));
		dispatcher.dispatch<AppRenderEvent>(BIND_EVENT_FN(Game::OnRender));
		dispatcher.dispatch<MouseButtonPresedEvent>(BIND_EVENT_FN(Game::OnMouseButtonPressed));
		dispatcher.dispatch<MouseMovedEvent>(BIND_EVENT_FN(Game::OnMouseMoved));
		dispatcher.dispatch<MouseScrolledEvent>(BIND_EVENT_FN(Game::OnMouseWheel));
		dispatcher.dispatch<KeyPressedEvent>(BIND_EVENT_FN(Game::OnKeyPressed));
		dispatcher.dispatch<KeyReleaseEvent>(BIND_EVENT_FN(Game::OnKeyReleased));
		//dispatcher.dispatch<WindowClosedEvent>(BIND_EVENT_FN(Game::on_window_closed));
		dispatcher.dispatch<WindowResizedEvent>(BIND_EVENT_FN(Game::OnResize));



		// Push events down to all of the layers.
		for (auto it = m_layerStack.end(); it != m_layerStack.begin();)
		{
 			(*--it)->OnEvent(e);
			if (e.get_handled())
				break;
		}
		return true;
	}

	bool Game::OnUpdate(AppUpdateEvent& e)
	{
		return true;
	}

	bool Game::OnRender(AppRenderEvent& e)
	{
		return true;
	}

	bool Game::OnKeyPressed(KeyPressedEvent& e)
	{
		// By default, do nothing.
		return true;
	}

	bool Game::OnKeyReleased(KeyReleaseEvent& e)
	{
		// By default, do nothing.
		return true;
	}

	bool Game::OnMouseMoved(MouseMovedEvent& e)
	{
		// By default, do nothing.
		return true;
	}

	bool Game::OnMouseButtonPressed(MouseButtonPresedEvent& e)
	{
		// By default, do nothing.
		return true;
	}

	bool Game::OnMouseButtonReleased(MouseButtonReleasedEvent& e)
	{
		// By default, do nothing.
		return true;
	}

	bool Game::OnMouseWheel(MouseScrolledEvent& e)
	{
		// By default, do nothing.
		return true;
	}

	bool Game::OnResize(WindowResizedEvent& e)
	{
		m_Width = e.get_width();
		m_Height = e.get_height();

		return true;
	}

	void Game::OnWindowDestroy()
	{
		// If the Window which we are registered to is 
		// destroyed, then any resources which are associated 
		// to the window must be released.
		UnloadContent();
	}

}