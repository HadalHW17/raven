#pragma once
#include "Raven/Raven.h"
#include "Renderer/CommandList.h"

#include <vector>
#include <memory>

#include <DirectXMath.h>

namespace Raven
{
	class Mesh;
	class RAVEN_API SceneObject
	{
	public:
		static std::unique_ptr<SceneObject> LoadObject(CommandList& commandList, std::string filePath);

		void Render(CommandList& commandList);

		inline DirectX::XMMATRIX GetWorldMatrix();

		//---------------------------------Setters for position------------------------------
		inline void SetPosition(DirectX::XMFLOAT3 position) { m_Position = position; }
		inline void SetPositionX(float x) { m_Position.x = x; }
		inline void SetPositionY(float y) { m_Position.y = y; }
		inline void SetPositionZ(float z) { m_Position.z = z; }
		//-----------------------------------------------------------------------------------

		//----------------------------------Setters for rotation-----------------------------
		inline void SetRotation(DirectX::XMFLOAT3 rotation) { m_Rotation = rotation; }
		inline void SetRotationX(float x) { m_Rotation.x = x; }
		inline void SetRotationY(float y) { m_Rotation.y = y; }
		inline void SetRotationZ(float z) { m_Rotation.z = z; }
		//-----------------------------------------------------------------------------------

		//----------------------------------Setters for scale--------------------------------
		inline void SetScale(DirectX::XMFLOAT3 scale) { m_Scale = scale; }
		inline void SetScaleX(float x) { m_Scale.x = x; }
		inline void SetScaleY(float y) { m_Scale.y = y; }
		inline void SetScaleZ(float z) { m_Scale.z = z; }
		//-----------------------------------------------------------------------------------


		inline std::vector<std::unique_ptr<Mesh>>& GetMeshes() { return m_Meshes; }
		private:
		SceneObject();
		SceneObject(const SceneObject& copy) = delete;
		SceneObject& operator = (const SceneObject&) = delete;
		virtual ~SceneObject();
		friend struct std::default_delete<SceneObject>;

		std::vector<std::unique_ptr<Mesh>> m_Meshes;

		DirectX::XMFLOAT3 m_Position;
		DirectX::XMFLOAT3 m_Rotation;
		DirectX::XMFLOAT3 m_Scale;
	};
}