#include "rvpch.h"

#include "SceneObject.h"
#include "Renderer/Mesh.h"

/* assimp include files. These three are usually needed. */
#include <assimp/mesh.h>
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <filesystem>

namespace Raven
{
	std::unique_ptr<SceneObject> SceneObject::LoadObject(CommandList& commandList, std::string filePath)
	{
		fs::path fs(filePath);
		if (!fs::exists(fs))
		{
			throw std::exception("Scene file not found.");
		}
		fs::path pp = fs.parent_path();
		std::wstring parentPath(pp.c_str());
		std::unique_ptr<SceneObject> rvObject(new SceneObject());
		// Load mesh from file.
		auto scene = aiImportFile(filePath.c_str(), aiProcessPreset_TargetRealtime_MaxQuality);
		auto materials = scene->mMaterials;
		for (int m_num = 0; m_num < scene->mNumMeshes; ++m_num)
		{
			auto mesh = scene->mMeshes[m_num];
			Texture meshTexture;
			Texture normalTexture;
			if (mesh->mMaterialIndex > 0 && mesh->mMaterialIndex < scene->mNumMaterials)
			{
				aiString textPath;
				// Load diffuse texture.
				if(materials[mesh->mMaterialIndex]->GetTexture(aiTextureType_DIFFUSE,0, &textPath) == AI_SUCCESS)
					commandList.LoadTextureFromFile(meshTexture,parentPath + L"/" + std::wstring(textPath.C_Str(), textPath.C_Str() + textPath.length));
				else
				{
					commandList.LoadTextureFromFile(meshTexture, L"Assets/Textures/DefaultWhite.bmp");
				}

				// Load normal map
				if (materials[mesh->mMaterialIndex]->GetTexture(aiTextureType_HEIGHT,0, &textPath) == AI_SUCCESS)
					commandList.LoadTextureFromFile(normalTexture, parentPath + L"/" + std::wstring(textPath.C_Str(), textPath.C_Str() + textPath.length));
				else
				{
					commandList.LoadTextureFromFile(normalTexture, L"Assets/Textures/DefaultWhite.bmp");
				}
			}
			else
			{
				commandList.LoadTextureFromFile(meshTexture, L"Assets/Textures/DefaultWhite.bmp");
				commandList.LoadTextureFromFile(normalTexture, L"Assets/Textures/DefaultWhite.bmp");
			}
			rvObject->m_Meshes.push_back(Mesh::ConvertFromAssimp(commandList, mesh, true, meshTexture, normalTexture));
		}
		return rvObject;
	}

	void SceneObject::Render(CommandList& commandList)
	{
		for (auto& mesh : m_Meshes)
		{
			mesh->Render(commandList);
		}
	}
	inline DirectX::XMMATRIX SceneObject::GetWorldMatrix()
	{
		DirectX::XMMATRIX translationMatrix = XMMatrixTranslation(m_Position.x, m_Position.y, m_Position.z);
		XMMATRIX rotationX = XMMatrixRotationX(XMConvertToRadians(m_Rotation.x));
		XMMATRIX rotationY = XMMatrixRotationX(XMConvertToRadians(m_Rotation.y));
		XMMATRIX rotationZ = XMMatrixRotationX(XMConvertToRadians(m_Rotation.z));

		XMMATRIX rotationMatrix = rotationX * rotationY * rotationZ;
		XMMATRIX scaleMatrix = XMMatrixScaling(m_Scale.x, m_Scale.y, m_Scale.z);

		return scaleMatrix * rotationMatrix * translationMatrix;
	}
	SceneObject::SceneObject()
		: m_Position({0.f, 0.f, 0.f})
		, m_Rotation({0.f, 0.f, 0.f})
		, m_Scale({1.f, 1.f,1.f})
	{
	}
	SceneObject::~SceneObject()
	{
	}
}