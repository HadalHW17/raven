#pragma once
#include "Event.h"
#include <sstream>
#include "Raven/Raven.h"

namespace Raven
{
	class RAVEN_API KeyEvent : public Event
	{
	public:
		EVENT_CLASS_CATEGORY(EventCategoryKeybord | EventCategoryInput)
		KeyCode::Key    m_Key;    // The Key Code that was pressed or released.
		unsigned int    m_Char;   // The 32-bit character code that was pressed. This value will be 0 if it is a non-printable character.
		bool            m_Control;// Is the Control modifier pressed
		bool            m_Shift;  // Is the Shift modifier pressed
		bool            m_Alt;    // Is the Alt modifier pressed
	protected:
		KeyEvent(KeyCode::Key key, unsigned int c, bool control, bool shift, bool alt)
			:m_Key(key),
			m_Char(c),
			m_Control(control),
			m_Shift(shift),
			m_Alt(alt){}


	};

	class RAVEN_API KeyPressedEvent : public KeyEvent
	{
	public:
		KeyPressedEvent(KeyCode::Key key, unsigned int c, bool control, bool shift, bool alt, int repeat_count)
			:KeyEvent(key,c, control, shift, alt), m_repeat_count(repeat_count) {}

		inline int get_repeat_cunt() const { return m_repeat_count; }
		std::string to_string() const override
		{
			std::stringstream ss;
			ss << "KeyPressedEvent: " << m_Char << " (" << m_repeat_count << ") repeats";
			return ss.str();
		}
		EVENT_CLASS_TYPE(KeyPressed);
	private:
		int m_repeat_count;	// Number of times key was pressed without releasing it.
	};


	class RAVEN_API KeyReleaseEvent : public KeyEvent
	{
	public:
		KeyReleaseEvent(KeyCode::Key key, unsigned int c, bool control, bool shift, bool alt)
			:KeyEvent(key, c, control, shift, alt) {}

		std::string to_string() const override
		{
			std::stringstream ss;
			ss << "KeyReleaseEvent: " << m_Char;
			return ss.str();
		}
		EVENT_CLASS_TYPE(KeyReleased)
	};

	class RAVEN_API KeyTypedEvent : public KeyEvent
	{
	public:
		KeyTypedEvent(KeyCode::Key key, unsigned int c, bool control, bool shift, bool alt)
			:KeyEvent(key, c, control, shift, alt) {}

		std::string to_string() const override
		{
			std::stringstream ss;
			ss << "KeyTypedEvent: " << m_Char;
			return ss.str();
		}
		EVENT_CLASS_TYPE(KeyTyped);
	private:

	};
}