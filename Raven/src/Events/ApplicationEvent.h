#pragma once

#include "Event.h"


namespace Raven
{
	class RAVEN_API WindowClosedEvent : public Event
	{
	public:
		WindowClosedEvent() {}
		EVENT_CLASS_TYPE(WindowClosed)
		EVENT_CLASS_CATEGORY(EventCategoryApplication)
	};

	class RAVEN_API AppTickEvent : public Event
	{
	public:
		AppTickEvent() {}
		EVENT_CLASS_TYPE(AppTick)
		EVENT_CLASS_CATEGORY(EventCategoryApplication)
	};

	class RAVEN_API AppUpdateEvent : public Event
	{
	public:
		AppUpdateEvent(double delta_time, double total_time, uint64_t frameCount):
		m_ElapsedTime(delta_time),
		m_TotalTime(total_time),
		m_frameCount(frameCount){}
		EVENT_CLASS_TYPE(AppUpdate)
		EVENT_CLASS_CATEGORY(EventCategoryApplication)

		double m_ElapsedTime;
		double m_TotalTime;
		uint64_t m_frameCount;
	};

	class RAVEN_API AppRenderEvent : public Event
	{
	public:
		AppRenderEvent(double delta_time, double total_time, uint64_t frameCount):
		m_ElapsedTime(delta_time),
		m_TotalTime(total_time),
		m_frameCount(frameCount){}
		EVENT_CLASS_TYPE(AppRender)
		EVENT_CLASS_CATEGORY(EventCategoryApplication)

		double m_ElapsedTime;
		double m_TotalTime;
		uint64_t m_frameCount;
	};

	class RAVEN_API WindowResizedEvent : public Event
	{
	public:
		WindowResizedEvent(size_t width, size_t heigth) :
			m_width(width), m_heigth(heigth) {}

		inline int get_width() const { return m_width; }
		inline int get_height() const { return m_heigth; }
		std::string to_string() const override
		{
			std::stringstream ss;
			ss << "WidndowReseizedEvent: " << m_width << " x " << m_heigth;
			return ss.str();
		}
		EVENT_CLASS_TYPE(WindowResize)
		EVENT_CLASS_CATEGORY(EventCategoryApplication)

	private:
		int m_width, m_heigth;
	};


}