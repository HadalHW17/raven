#pragma once

#include "Event.h"

namespace Raven
{
	class RAVEN_API MouseMovedEvent : public Event
	{
	public:
		MouseMovedEvent(bool leftButton, bool middleButton, bool rightButton, bool control, bool shift, int x, int y)
			:m_LeftButton(leftButton), 
			m_MiddleButton(middleButton),
			m_RightButton(rightButton),
			m_Control(control),
			m_Shift(shift),
			m_X(x),
			m_Y(y)
		{}


		std::string to_string() const override 
		{
			std::stringstream ss;
			ss << "MouseMoveEvent: " << "[" << m_X << " , " << m_Y << "]";
			return ss.str();
		}

		EVENT_CLASS_TYPE(MouseMoved)
		EVENT_CLASS_CATEGORY(EventCategoryMouse | EventCategoryInput)

		bool m_LeftButton;    // Is the left mouse button down?
		bool m_MiddleButton;  // Is the middle mouse button down?
		bool m_RightButton;   // Is the right mouse button down?
		bool m_Control;       // Is the CTRL key down?
		bool m_Shift;         // Is the Shift key down?

		int m_X;              // The X-position of the cursor relative to the upper-left corner of the client area.
		int m_Y;              // The Y-position of the cursor relative to the upper-left corner of the client area.
		int m_RelX;           // How far the mouse moved since the last event.
		int m_RelY;           // How far the mouse moved since the last event.
	private:
	};

	class RAVEN_API MouseScrolledEvent : public Event
	{
	public:
		MouseScrolledEvent(float wheelDelta, bool leftButton, bool middleButton, bool rightButton, bool control, bool shift, int x, int y)
			:m_WheelDelta(wheelDelta),
			m_LeftButton(leftButton),
			m_MiddleButton(middleButton),
			m_RightButton(rightButton),
			m_Control(control),
			m_Shift(shift),
			m_Xoffset(x),
			m_Yoffset(y)
		{}

		std::string to_string() const override
		{
			std::stringstream ss;
			ss << "MouseScrolledEvent: " << m_Xoffset << ", " << m_Yoffset;
			return ss.str();
		}
		EVENT_CLASS_TYPE(MouseScrolled)
		EVENT_CLASS_CATEGORY(EventCategoryMouse | EventCategoryInput)
		float m_WheelDelta;   // How much the mouse wheel has moved. A positive value indicates that the wheel was moved to the right. A negative value indicates the wheel was moved to the left.
		bool m_LeftButton;    // Is the left mouse button down?
		bool m_MiddleButton;  // Is the middle mouse button down?
		bool m_RightButton;   // Is the right mouse button down?
		bool m_Control;       // Is the CTRL key down?
		bool m_Shift;         // Is the Shift key down?

		int m_Xoffset;              // The X-position of the cursor relative to the upper-left corner of the client area.
		int m_Yoffset;              // The Y-position of the cursor relative to the upper-left corner of the client area.
	};

	class RAVEN_API MouseButtonEvent : public Event
	{
	public:
		std::string to_string() const override
		{
			std::stringstream ss;
			ss << "MouseButtonEvent: " << m_Button;
			return ss.str();
		}
		EVENT_CLASS_CATEGORY(EventCategoryMouse | EventCategoryInput)

		KeyCode::MouseButton m_Button; // The mouse button that was pressed or released.
		bool m_LeftButton;    // Is the left mouse button down?
		bool m_MiddleButton;  // Is the middle mouse button down?
		bool m_RightButton;   // Is the right mouse button down?
		bool m_Control;       // Is the CTRL key down?
		bool m_Shift;         // Is the Shift key down?

		int m_X;              // The X-position of the cursor relative to the upper-left corner of the client area.
		int m_Y;              // The Y-position of the cursor relative to the upper-left corner of the client area.
	protected: 
		MouseButtonEvent(KeyCode::MouseButton buttonID, bool leftButton, bool middleButton, bool rightButton, bool control, bool shift, int x, int y)
			:m_Button(buttonID),
			m_LeftButton(leftButton),
			m_MiddleButton(middleButton),
			m_RightButton(rightButton),
			m_Control(control),
			m_Shift(shift),
			m_X(x),
			m_Y(y)
		{}
	};

	class RAVEN_API MouseButtonPresedEvent : public MouseButtonEvent
	{
	public:
		MouseButtonPresedEvent(KeyCode::MouseButton buttonID, bool leftButton, bool middleButton, bool rightButton, bool control, bool shift, int x, int y)
			:MouseButtonEvent(buttonID, leftButton, middleButton, rightButton, control, shift, x, y) {}

		std::string to_string() const override
		{
			std::stringstream ss;
			ss << "MouseButtonPressedEvent: " << m_Button;
			return ss.str();
		}
		EVENT_CLASS_TYPE(MouseButtonPressed)
	};

	class RAVEN_API MouseButtonReleasedEvent : public MouseButtonEvent
	{
	public:
		MouseButtonReleasedEvent(KeyCode::MouseButton buttonID, bool leftButton, bool middleButton, bool rightButton, bool control, bool shift, int x, int y)
			:MouseButtonEvent(buttonID, leftButton, middleButton, rightButton, control, shift, x, y) {}

		std::string to_string() const override
		{
			std::stringstream ss;
			ss << "MouseButtonReleasedEvent: " << m_Button;
			return ss.str();
		}

		EVENT_CLASS_TYPE(MouseButtonReleased)
	};
}