#pragma once
#include "Event.h"
#include <sstream>

namespace Raven
{

	class RAVEN_API GenerateChunkEvent : public Event
	{
	public:
		GenerateChunkEvent() {}

		std::string to_string() const override
		{
			std::stringstream ss;
			ss << "GenerateChunkEvent: ";
			return ss.str();
		}

		EVENT_CLASS_TYPE(GenerateChunkRequest)
		EVENT_CLASS_CATEGORY(EventCategoryWorld)

	private:
	};

	class RAVEN_API ChunkExitedEvent : public Event
	{
	public:
		ChunkExitedEvent(float3 new_position)
			:position(new_position) {}

		inline float3 get_position() const { return position; }

		std::string to_string() const override
		{
			std::stringstream ss;
			ss << "ChunkExitedEvent: " << "[" << position.x << " , " << position.y << "," << position.z << "]";
			return ss.str();
		}

		EVENT_CLASS_TYPE(ChunkExited)
		EVENT_CLASS_CATEGORY(EventCategoryWorld)

	private:
		float3 position;
	};

	class RAVEN_API SaveChunkEvent : public Event
	{
	public:
		SaveChunkEvent() {}

		std::string to_string() const override
		{
			std::stringstream ss;
			ss << "SaveChunkEvent: ... ";
			return ss.str();
		}

		EVENT_CLASS_TYPE(SaveMapRequested)
		EVENT_CLASS_CATEGORY(EventCategoryWorld)

	private:
	};

	class RAVEN_API LoadChunkEvent : public Event
	{
	public:
		LoadChunkEvent(std::string filename)
			:m_filename(filename) {}

		inline std::string get_filename() const { return m_filename; }

		std::string to_string() const override
		{
			std::stringstream ss;
			ss << "LoadChunkEvent: " << m_filename;
			return ss.str();
		}

		EVENT_CLASS_TYPE(LoadMapRequested)
		EVENT_CLASS_CATEGORY(EventCategoryWorld)

	private:
		std::string m_filename;
	};
}