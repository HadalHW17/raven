#pragma once

// For use only by external applications
#include <stdio.h>
#include "Engine/Application.h"

////------------------Layers-------------------------
//#include "Layers/Layer.h"
//#include "Layers/UILayer.h"
////-------------------------------------------------
//
////---------------Inputs----------------------------
//#include "Engine/Input.h"
//#include "Engine/KeyCodes.h"
//#include "Engine/MouseButtonCodes.h"
////-------------------------------------------------

//-----------------Entry point---------------------
#include "Raven/EntryPoint.h"
//-------------------------------------------------
