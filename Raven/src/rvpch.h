#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <shellapi.h> // For CommandLineToArgvW

// The min/max macros conflict with like-named member functions.
// Only use std::min and std::max defined in <algorithm>.
#if defined(min)
#undef min
#endif

#if defined(max)
#undef max
#endif

// In order to define a function called CreateWindow, the Windows macro needs to
// be undefined.
#if defined(CreateWindow)
#undef CreateWindow
#endif

// Windows Runtime Library. Needed for Microsoft::WRL::ComPtr<> template class.
#include <wrl.h>
using namespace Microsoft::WRL;

// DirectX 12 specific headers.
#include "d3dx12.h"
#include <dxgi1_6.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include <DirectXTex.h>

using namespace DirectX;

#include <Engine/d3dx12.h>


#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>
#include <functional>
#include <utility>
#include <cmath>
#include <limits>
#include <random>

#include <string>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <cassert>


#include <thread>
#include <fstream>
#include <filesystem>


namespace fs = std::experimental::filesystem;


#include "Engine/KeyCodes.h"
#include "Raven/Raven.h"

#include "Events/Event.h"
#include "Events/KeyEvent.h"
#include "Events/ApplicationEvent.h"
#include "Events/MouseEvent.h"

#include <stdio.h>

#ifdef RE_PLATFORM_WINDOWS
	#include <Windows.h>
#endif // RE_PLATFORM_WINDOWS

// Assimp header files.
#include <assimp/Importer.hpp>
#include <assimp/Exporter.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/mesh.h>
#include <assimp/anim.h>
#include <assimp/config.h>


#include "Engine/Helpers.h"