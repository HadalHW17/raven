#pragma once


#include "Events/Event.h"
#include "Events/KeyEvent.h"
#include "Events/MouseEvent.h"
#include "Events/ApplicationEvent.h"
#include "Raven/Raven.h"

namespace Raven
{
	class RAVEN_API RLayer
	{
	public:
		RLayer(const std::string& name = "Name a layer");
		virtual ~RLayer();

		virtual void OnAttach() {}
		virtual void OnDetach() {}
		virtual void OnUpdate() {}
		virtual void OnEvent(Event& event);

		inline const std::string& get_name() const { return m_debug_name; }
		inline void set_name(std::string& name) { m_debug_name = name; }

		std::string m_debug_name;
	private:
		virtual bool OnMouseButtonPressed(MouseButtonPresedEvent& e) = 0;
		virtual bool OnMouseButtonRelseased(MouseButtonReleasedEvent& e) = 0;
		virtual bool OnMouseMoved(MouseMovedEvent& e) = 0;
		virtual bool OnMouseScrolled(MouseScrolledEvent& e) = 0;
		virtual bool OnWindowReseized(WindowResizedEvent& e) = 0;
		virtual bool OnKeyReleased(KeyReleaseEvent& e) = 0;
		virtual bool OnKeyPressed(KeyPressedEvent& e) = 0;
		virtual bool OnKeyTyped(KeyTypedEvent& e) = 0;
		virtual bool OnRendered(AppRenderEvent& e) = 0;
		virtual bool OnUpdated(AppUpdateEvent& e) = 0;

	protected:

	};

}

//#include <Meta.h>
//namespace meta {
//
//	template <>
//	inline auto registerMembers<Raven::RLayer>()
//	{
//		return members(
//			member("m_debug_name", &Raven::RLayer::m_debug_name)
//		);
//	}
//} // end of na