#include "rvpch.h"


#include "UILayer.h"
#include "Engine/Window.h"
#include "Engine/Application.h"
#include "Engine/Game.h"
#include "Renderer/CommandQueue.h"
#include "Renderer/CommandList.h"

namespace Raven
{

	RUILayer::RUILayer() :
		RLayer("UI Layer"), m_time(0)
	{
	}

	RUILayer::~RUILayer()
	{
	}

	void RUILayer::OnAttach()
	{
		m_GUI.Initialize(Application::Get().GetGameRef()->GetWindow());
	}

	void RUILayer::OnDetach()
	{
		m_GUI.Destroy();
	}


	void RUILayer::OnUpdate()
	{

	}

	void RUILayer::OnEvent(Event& event)
	{
		super::OnEvent(event);
	}

	bool RUILayer::OnMouseButtonPressed(MouseButtonPresedEvent& e)
	{
		return true;
	}

	bool RUILayer::OnMouseButtonRelseased(MouseButtonReleasedEvent& e)
	{
		return true;
	}

	bool RUILayer::OnMouseMoved(MouseMovedEvent& e)
	{
		return true;
	}

	bool RUILayer::OnMouseScrolled(MouseScrolledEvent& e)
	{
		return true;
	}

	bool RUILayer::OnWindowReseized(WindowResizedEvent& e)
	{
		return true;
	}

	bool RUILayer::OnKeyReleased(KeyReleaseEvent& e)
	{
		return true;
	}

	bool RUILayer::OnKeyPressed(KeyPressedEvent& e)
	{
		return true;
	}

	bool RUILayer::OnKeyTyped(KeyTypedEvent& e)
	{
		return true;
	}
	bool RUILayer::OnRendered(AppRenderEvent& e)
	{


		return true;
	}
	bool RUILayer::OnUpdated(AppUpdateEvent& e)
	{
		m_GUI.NewFrame();


		for (const auto& overlay : m_overlays)
		{
			overlay();
		}
		return true;
	}
}