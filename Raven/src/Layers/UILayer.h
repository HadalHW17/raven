#pragma once


#include "Layers/Layer.h"

#include "Engine/Application.h"
#include "Events/KeyEvent.h"
#include "Events/MouseEvent.h"
#include "Events/ApplicationEvent.h"
#include "Raven/Raven.h"
#include <vector>
#include "Renderer/GUI.h"

namespace Raven
{
	class RAVEN_API RUILayer : public RLayer
	{

	public:
		using super = RLayer;
		RUILayer();
		virtual ~RUILayer();

		inline GUI& GetGUI() { return m_GUI; }

		virtual void OnAttach() override;
		virtual void OnDetach() override;
		virtual void OnUpdate() override;
		virtual void OnEvent(Event& event) override;
		inline bool is_visible() const { return m_is_visible; }
		inline void AddOverlay(std::function<void()> ov) { m_overlays.push_back(ov); }

		void render_menu();
		std::vector<std::function<void()>> m_overlays;

	protected:
		virtual bool OnMouseButtonPressed(MouseButtonPresedEvent &e) override;
		virtual bool OnMouseButtonRelseased(MouseButtonReleasedEvent &e) override;
		virtual bool OnMouseMoved(MouseMovedEvent &e) override;
		virtual bool OnMouseScrolled(MouseScrolledEvent &e) override;
		virtual bool OnWindowReseized(WindowResizedEvent &e) override;
		virtual bool OnKeyReleased(KeyReleaseEvent& e) override;
		virtual bool OnKeyPressed(KeyPressedEvent& e) override;
		virtual bool OnKeyTyped(KeyTypedEvent& e) override;
		virtual bool OnRendered(AppRenderEvent& e) override;
		virtual bool OnUpdated(AppUpdateEvent& e) override;


	private:

		float m_time;
		bool m_is_visible = false;

		GUI m_GUI;

		// Store pointer to the owning window.
	};
}
