#include "rvpch.h"


#include "Layer.h"

#include "Events/ApplicationEvent.h"

namespace Raven
{
	RLayer::RLayer(const std::string& name)
		:
		m_debug_name(name)
	{
	}

	RLayer::~RLayer()
	{
	}

	void RLayer::OnEvent(Event& event)
	{
		EventDispatcher dispatcher(event);

		dispatcher.dispatch<MouseButtonPresedEvent>(BIND_EVENT_FN(RLayer::OnMouseButtonPressed));
		dispatcher.dispatch<MouseButtonReleasedEvent>(BIND_EVENT_FN(RLayer::OnMouseButtonRelseased));
		dispatcher.dispatch<MouseMovedEvent>(BIND_EVENT_FN(RLayer::OnMouseMoved));
		dispatcher.dispatch<MouseScrolledEvent>(BIND_EVENT_FN(RLayer::OnMouseScrolled));
		dispatcher.dispatch<WindowResizedEvent>(BIND_EVENT_FN(RLayer::OnWindowReseized));
		dispatcher.dispatch<KeyReleaseEvent>(BIND_EVENT_FN(RLayer::OnKeyReleased));
		dispatcher.dispatch<KeyPressedEvent>(BIND_EVENT_FN(RLayer::OnKeyPressed));
		dispatcher.dispatch<KeyTypedEvent>(BIND_EVENT_FN(RLayer::OnKeyTyped));
		dispatcher.dispatch<AppRenderEvent>(BIND_EVENT_FN(RLayer::OnRendered));
		dispatcher.dispatch<AppUpdateEvent>(BIND_EVENT_FN(RLayer::OnUpdated));
	}
}