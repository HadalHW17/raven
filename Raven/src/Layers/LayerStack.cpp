#include "rvpch.h"


#include "LayerStack.h"

Raven::RLayerStack::RLayerStack()
{
	m_layer_insert = m_layers.begin();
}

Raven::RLayerStack::~RLayerStack()
{
	for (auto layer : m_layers)
		delete layer;
}

void Raven::RLayerStack::push_layer(RLayer* layer)
{
	m_layer_insert = m_layers.emplace(m_layer_insert, layer);
}

void Raven::RLayerStack::push_overlay(RLayer* layer)
{
	m_layers.emplace_back(layer);
}

void Raven::RLayerStack::pop_layer(RLayer* layer)
{
	auto it = std::find(m_layers.begin(), m_layers.end(), layer);
	if (it != m_layers.end())
	{
		m_layers.erase(it);
		m_layer_insert--;
	}
}

void Raven::RLayerStack::pop_overlay(RLayer* layer)
{
	auto it = std::find(m_layers.begin(), m_layers.end(), layer);
	if (it != m_layers.end())
	{
		m_layers.erase(it);
	}
}
