#pragma once
#include "Engine/Input.h"

namespace Raven
{
	class RAVEN_API WindowsInput :
		public Input
	{
	protected:
		virtual bool is_key_pressed_impl(int keycode) override;
		virtual bool is_mouse_button_pressed_impl(int button) override;
		virtual float get_mouse_x_impl() override;
		virtual float get_mouse_y_impl() override;
		virtual std::pair<float, float> get_mouse_pos_impl() override;
	};

}